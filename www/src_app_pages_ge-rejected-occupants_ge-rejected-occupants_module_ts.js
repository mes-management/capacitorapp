"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_ge-rejected-occupants_ge-rejected-occupants_module_ts"],{

/***/ 46606:
/*!*************************************************************************************!*\
  !*** ./src/app/pages/ge-rejected-occupants/ge-rejected-occupants-routing.module.ts ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeRejectedOccupantsPageRoutingModule": () => (/* binding */ GeRejectedOccupantsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 65485);
/* harmony import */ var _ge_rejected_occupants_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ge-rejected-occupants.page */ 49949);




const routes = [
    {
        path: '',
        component: _ge_rejected_occupants_page__WEBPACK_IMPORTED_MODULE_0__.GeRejectedOccupantsPage
    }
];
let GeRejectedOccupantsPageRoutingModule = class GeRejectedOccupantsPageRoutingModule {
};
GeRejectedOccupantsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], GeRejectedOccupantsPageRoutingModule);



/***/ }),

/***/ 75560:
/*!*****************************************************************************!*\
  !*** ./src/app/pages/ge-rejected-occupants/ge-rejected-occupants.module.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeRejectedOccupantsPageModule": () => (/* binding */ GeRejectedOccupantsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38143);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 31777);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 95472);
/* harmony import */ var _ge_rejected_occupants_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ge-rejected-occupants-routing.module */ 46606);
/* harmony import */ var _ge_rejected_occupants_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ge-rejected-occupants.page */ 49949);







let GeRejectedOccupantsPageModule = class GeRejectedOccupantsPageModule {
};
GeRejectedOccupantsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _ge_rejected_occupants_routing_module__WEBPACK_IMPORTED_MODULE_0__.GeRejectedOccupantsPageRoutingModule
        ],
        declarations: [_ge_rejected_occupants_page__WEBPACK_IMPORTED_MODULE_1__.GeRejectedOccupantsPage]
    })
], GeRejectedOccupantsPageModule);



/***/ }),

/***/ 49949:
/*!***************************************************************************!*\
  !*** ./src/app/pages/ge-rejected-occupants/ge-rejected-occupants.page.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeRejectedOccupantsPage": () => (/* binding */ GeRejectedOccupantsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _ge_rejected_occupants_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ge-rejected-occupants.page.html?ngResource */ 55739);
/* harmony import */ var _ge_rejected_occupants_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ge-rejected-occupants.page.scss?ngResource */ 20899);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 65485);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 95472);
/* harmony import */ var src_app_providers_LoadingService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/providers/LoadingService */ 67145);
/* harmony import */ var src_app_providers_occupant_registration_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/providers/occupant-registration.service */ 78197);








let GeRejectedOccupantsPage = class GeRejectedOccupantsPage {
    constructor(OccupantService, route, loadingController, loading) {
        this.OccupantService = OccupantService;
        this.route = route;
        this.loadingController = loadingController;
        this.loading = loading;
        this.Occupantdata = {
            OccupantName: "",
            EmailID: "",
            MobileNumber: "",
            Location: "",
            Division: "",
            CreatedDate: "",
            ComplexName: "",
            BuildingName: "",
            AccomodationNo: "",
            Status: "",
        };
    }
    ngOnInit() {
        let MobileNumber = localStorage.getItem('mobileNumber');
        this.OccupantService.RejectedOccupantDetailsByMono(MobileNumber).subscribe(res => {
            if (res) {
                this.Occupantdata.OccupantName = res[0].name;
                this.Occupantdata.EmailID = res[0].emailID;
                this.Occupantdata.MobileNumber = res[0].mobileNumber;
                this.Occupantdata.Location = res[0].location;
                this.Occupantdata.Division = res[0].officeType;
                this.Occupantdata.CreatedDate = res[0].createdDate;
                this.Occupantdata.ComplexName = res[0].complexName;
                this.Occupantdata.BuildingName = res[0].buildingName;
                this.Occupantdata.AccomodationNo = res[0].accommodationNumber;
                this.Occupantdata.Status = res[0].status;
            }
        });
    }
};
GeRejectedOccupantsPage.ctorParameters = () => [
    { type: src_app_providers_occupant_registration_service__WEBPACK_IMPORTED_MODULE_3__.OccupantRegistrationService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: src_app_providers_LoadingService__WEBPACK_IMPORTED_MODULE_2__.LoadingService }
];
GeRejectedOccupantsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-ge-rejected-occupants',
        template: _ge_rejected_occupants_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        providers: [src_app_providers_occupant_registration_service__WEBPACK_IMPORTED_MODULE_3__.OccupantRegistrationService],
        styles: [_ge_rejected_occupants_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], GeRejectedOccupantsPage);



/***/ }),

/***/ 20899:
/*!****************************************************************************************!*\
  !*** ./src/app/pages/ge-rejected-occupants/ge-rejected-occupants.page.scss?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = ".title {\n  color: aliceblue;\n  text-align: center;\n  font: bold;\n}\n\n.grad {\n  background-image: linear-gradient(to right, #4E2AFF, #42F5FF);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdlLXJlamVjdGVkLW9jY3VwYW50cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0gsVUFBQTtBQUNEOztBQUVFO0VBRUUsNkRBQUE7QUFBSiIsImZpbGUiOiJnZS1yZWplY3RlZC1vY2N1cGFudHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRpdGxle1xyXG4gICAgY29sb3I6IGFsaWNlYmx1ZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuIGZvbnQ6IGJvbGQ7XHJcbiAgXHJcbiAgfVxyXG4gIC5ncmFke1xyXG4gICAvLyBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzRFMkFGRiAsIzQyRjVGRik7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM0RTJBRkYgLCM0MkY1RkYpO1xyXG4gIH0iXX0= */";

/***/ }),

/***/ 55739:
/*!****************************************************************************************!*\
  !*** ./src/app/pages/ge-rejected-occupants/ge-rejected-occupants.page.html?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar class=\"grad\" color=\"transparent\">\n    <ion-buttons slot=\"start\" >\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Rejected Occupant Details</ion-title>\n  </ion-toolbar>\n\n\n<style>\n  .button-md {\n  height: 3.6rem !important;\n  font-size: 1em;\n  float: inherit;\n}\n</style>\n</ion-header>\n\n\n<ion-content padding>\n <div>\n    <ion-item color=\"transparent\" lines=\"none\" style=\"background-color: lightgrey;\">\n      <h4 class=\"headerr\">Personal Information:</h4>\n    </ion-item> \n    <ion-card class=\"card-diplay\"> \n      <span>Occupant Name:{{this.Occupantdata.OccupantName}} </span><br>\n      <span>Mobile Number:{{this.Occupantdata.MobileNumber}} </span><br>\n\n        <!-- <span>EmailID:{{this.Occupantdata.EmailID}} </span> -->\n        <span>Requested Date:{{this.Occupantdata.CreatedDate}}</span><br>\n        <span>Status: {{this.Occupantdata.Status}}</span><br>\n       \n    </ion-card>\n  </div>\n\n  \n  <div>\n    <ion-item color=\"transparent\" lines=\"none\" style=\"background-color: lightgrey;\">\n      <h4 class=\"headerr\">Accomodation Details:</h4>\n    </ion-item> \n    <ion-card class=\"card-diplay-detail\"> \n        <span>Location: {{this.Occupantdata.Location}}</span><br>\n        <span>Division:{{this.Occupantdata.Division}} </span><br>\n        <span>Complex:{{this.Occupantdata.ComplexName}}</span><br>\n          \n        <span>Building Name: {{this.Occupantdata.BuildingName}}</span><br>\n        <span>Accomodation Number: {{this.Occupantdata.AccomodationNo}}</span><br><br>\n       \n    </ion-card>\n  </div>\n  <br>\n\n</ion-content>\n\n";

/***/ })

}]);
//# sourceMappingURL=src_app_pages_ge-rejected-occupants_ge-rejected-occupants_module_ts.js.map