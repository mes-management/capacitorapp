"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_dashboard_dashboard_module_ts"],{

/***/ 50425:
/*!*******************************************************!*\
  !*** ./src/app/dashboard/dashboard-routing.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashboardPageRoutingModule": () => (/* binding */ DashboardPageRoutingModule)
/* harmony export */ });
<<<<<<< HEAD
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 65485);
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard.page */ 65935);
=======
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 8163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 1109);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 5485);
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard.page */ 5935);
>>>>>>> 433948a77c4b4afad20f042cd316eb052ae61df0




const routes = [
    {
        path: '',
        component: _dashboard_page__WEBPACK_IMPORTED_MODULE_0__.DashboardPage
    }
];
let DashboardPageRoutingModule = class DashboardPageRoutingModule {
};
DashboardPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__.CUSTOM_ELEMENTS_SCHEMA]
    })
], DashboardPageRoutingModule);



/***/ }),

/***/ 34814:
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashboardPageModule": () => (/* binding */ DashboardPageModule)
/* harmony export */ });
<<<<<<< HEAD
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38143);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 31777);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 95472);
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard-routing.module */ 50425);
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.page */ 65935);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 65485);
=======
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 8163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 1109);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8143);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 1777);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 5472);
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard-routing.module */ 425);
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.page */ 5935);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 5485);
>>>>>>> 433948a77c4b4afad20f042cd316eb052ae61df0








const routes = [
    {
        path: 'dashboard',
        component: _dashboard_page__WEBPACK_IMPORTED_MODULE_1__.DashboardPage,
    },
];
let DashboardPageModule = class DashboardPageModule {
};
DashboardPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_0__.DashboardPageRoutingModule
        ],
        declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_1__.DashboardPage]
    }),
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        declarations: [
            _dashboard_page__WEBPACK_IMPORTED_MODULE_1__.DashboardPage,
        ],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes),
            // SharedStaticModule,
            //   IonicPageModule.forChild(DashboardPage),
        ],
    })
], DashboardPageModule);



/***/ }),

/***/ 65935:
/*!*********************************************!*\
  !*** ./src/app/dashboard/dashboard.page.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashboardPage": () => (/* binding */ DashboardPage)
/* harmony export */ });
<<<<<<< HEAD
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _dashboard_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard.page.html?ngResource */ 73957);
/* harmony import */ var _dashboard_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.page.scss?ngResource */ 4157);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ 90070);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 65485);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 89258);
=======
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 8163);
/* harmony import */ var _dashboard_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard.page.html?ngResource */ 3957);
/* harmony import */ var _dashboard_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.page.scss?ngResource */ 4157);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 1109);
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ 70);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 5485);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 9258);
>>>>>>> 433948a77c4b4afad20f042cd316eb052ae61df0







let DashboardPage = class DashboardPage {
    constructor(http, route) {
        this.http = http;
        this.route = route;
        this.locationData = { LocationID: '' };
        this.http.get('http://meswebapi.neemus.in/api/Location/GetLocationDetails').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.map)(res => res.json())).subscribe(data => {
            console.log(data);
            alert(JSON.stringify(data));
            this.locations = data;
            //data[0].acc_DetailsID;
            //this.optionsList=data;
        });
        // alert("dghfgh")
        // this.http.get('http://meswebapi.neemus.in/api/Complaint/GetRegisteredComplaintDetails').map(res => res.json()).subscribe(data => {
        //   console.log(data);
        //   //alert(JSON.stringify(data[0].acc_DetailsID))
        //   this.count=data[0].acc_DetailsID;
        //this.optionsList=data;
        //}
        //);
    }
    //event: Event
    RegisteredView() {
        this.route.navigate(['/viewregisteredcom']);
    }
    PendingView() {
        this.route.navigate(['/viewgependingcomplaints']);
    }
    onSelectChangeAccom(data) {
        this.locationData.LocationID = data.locationID;
        //alert(JSON.stringify(this.locationData))
        this.http.post('http://meswebapi.neemus.in/api/Complaint/GetRegisteredComplaintDetails', (this.locationData)).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.map)(res => res.json())).subscribe(data => {
            this.count = data[0].acc_DetailsID;
        }, (err) => {
            //reject(err);
        });
        //Pending Complaints
        this.http.post('http://meswebapi.neemus.in/api/Complaint/GetPendingComplaintDetails', (this.locationData)).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.map)(res => res.json())).subscribe(data => {
            this.Pendingcount = data[0].acc_DetailsID;
        }, (err) => {
            //reject(err);
        });
        //Resolved Complaints
        this.http.post('http://meswebapi.neemus.in/api/Complaint/GetResolvedComplaintDetails', (this.locationData)).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.map)(res => res.json())).subscribe(data => {
            this.Resolvedcount = data[0].acc_DetailsID;
        }, (err) => {
            //reject(err);
        });
    }
    // optionsFn()
    // {
    //   alert(this.gaming)
    // }
    // changelocation(datas)
    // {
    //   alert(JSON.stringify(datas))
    //   this.locationData.LocationID=datas;
    //   alert(JSON.stringify(this.locationData))
    // }
    ngOnInit() {
    }
};
DashboardPage.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_3__.Http },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router }
];
DashboardPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-dashboard',
        template: _dashboard_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_dashboard_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], DashboardPage);



/***/ }),

/***/ 4157:
/*!**********************************************************!*\
  !*** ./src/app/dashboard/dashboard.page.scss?ngResource ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJkYXNoYm9hcmQucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 73957:
/*!**********************************************************!*\
  !*** ./src/app/dashboard/dashboard.page.html?ngResource ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">  \r\n    <ion-buttons slot=\"start\">  \r\n      <ion-menu-button color=\"dark\"></ion-menu-button>   \r\n    </ion-buttons>  \r\n    <ion-title>  \r\n     DashBoard\r\n    </ion-title>  \r\n  </ion-toolbar>   \r\n</ion-header>\r\n<!-- <ion-header>\r\n\r\n  <ion-navbar>\r\n    <button ion-button menuToggle>\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n  </button>\r\n\r\n<ion-title></ion-title> \r\n<div class=\"pagelogo\">\r\n     \r\n    </div>\r\n  </ion-navbar>\r\n\r\n</ion-header> -->\r\n<ion-content>\r\n\r\n    <ion-item>\r\n      <ion-label>Location</ion-label>\r\n      <!-- <ion-label stacked> Service Type *</ion-label> -->\r\n          <!-- <ion-select placeholder=\"Select One\">\r\n            <ion-option *ngFor=\"let item of locations\" [value]=\"item\">{{item.locationName}}</ion-option>\r\n          </ion-select> -->\r\n\r\n          <!-- <ion-select \r\n      multiple=\"false\" \r\n      cancelText=\"Cancel\" \r\n      okText=\"Ok\"\r\n      (ionChange)=\"Getselected(item)\"\r\n    > -->\r\n    <!-- <ion-select name=\"location\" name=\"resident\" (ionChange)=\"onSelectChangeVisitor($event)\" >\r\n      <ion-select-option *ngFor=\"let dept of locations\" value=\"dept\"> {{dept.locationName}}</ion-select-option>\r\n   </ion-select> -->\r\n   <ion-select name=\"location\"  [interfaceOptions]=\"customPopoverOptions\" interface=\"popover\" (ionChange)=\"onSelectChangeAccom($event.target.value)\" >\r\n    <ion-select-option *ngFor=\"let accom of locations\" [value]=accom> {{accom.locationName}}</ion-select-option>\r\n </ion-select>\r\n    <!-- <ion-select  name=\"location\" (ionChange)=\"onSelectChangeVisitor($event)\" >\r\n      <ion-select-option *ngFor=\"let komp of locations\" [value]=\"komp\" >\r\n        {{ komp.locationName }}\r\n      </ion-select-option>\r\n    </ion-select> -->\r\n\r\n      <!-- <ion-select placeholder=\"Select One\">\r\n        <ion-select-option value=\"f\">Goa</ion-select-option>\r\n        <ion-select-option value=\"m\">Kochi</ion-select-option>\r\n      </ion-select> -->\r\n    </ion-item>\r\n\r\n    <ion-card >\r\n      <ion-list>\r\n        <ion-item>\r\n          <ion-thumbnail item-start>\r\n            <!-- <img src=\"/assets/imgs/hotel.jpg\"> -->\r\n          </ion-thumbnail>\r\n          <h2>{{ghn}}</h2>\r\n          <h6 style=\"color:green\">Registered Complaints</h6>\r\n          <button ion-button clear item-end >{{count}}</button>\r\n        </ion-item>\r\n        <ion-item>\r\n          <ion-button (click)=\"RegisteredView()\"> View </ion-button>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-card>\r\n\r\n\r\n  <ion-card  >\r\n    <ion-list>\r\n        <ion-item>\r\n          <ion-thumbnail item-start>\r\n            <!-- <img src=\"/assets/imgs/hotel.jpg\"> -->\r\n          </ion-thumbnail>\r\n          <h2>{{ghn}}</h2>\r\n          <h6 style=\"color:rgb(255, 0, 200)\">Pending Complaints</h6>\r\n          <!-- <h3 style=\"color:green\">Available Rooms  </h3> -->\r\n          <button ion-button clear item-end >{{Pendingcount}}</button>\r\n          <!-- <button ion-button clear item-end>{{arrLength1}}</button> -->\r\n        </ion-item>\r\n        <ion-item>\r\n          <ion-button (click)=\"PendingView()\"> View </ion-button>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-card>\r\n\r\n    <ion-card >\r\n      <ion-list>\r\n        <ion-item>\r\n          <ion-thumbnail item-start>\r\n            <!-- <img src=\"/assets/imgs/hotel.jpg\"> -->\r\n          </ion-thumbnail>\r\n          <h2>{{ghn}}</h2>\r\n          <h6 style=\"color:blue\">Resolved Complaints </h6>\r\n          <button ion-button clear item-end >{{Resolvedcount}}</button>\r\n          <!-- <button ion-button clear item-end >{{arrLength}}</button> -->\r\n        </ion-item>\r\n        <ion-item>\r\n          <ion-button (click)=\"ResolvedView()\"> View </ion-button>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-card>\r\n\r\n    \r\n\r\n    <ion-card >\r\n      <ion-list>\r\n        <ion-item>\r\n          <ion-thumbnail item-start>\r\n            <!-- <img src=\"/assets/imgs/hotel.jpg\"> -->\r\n          </ion-thumbnail>\r\n          <h2>{{ghn}}</h2>\r\n          <h6 style=\"color:rgb(243, 60, 54)\">Occupied/Vacated</h6>\r\n          <button ion-button clear item-end >{{arrLength}}</button>\r\n        </ion-item>\r\n        <ion-item>\r\n          <ion-button (click)=\"OccupiedView()\"> View </ion-button>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-card>\r\n\r\n    <!-- <ion-card >\r\n      <ion-list>\r\n        <ion-item>\r\n          <ion-thumbnail item-start>\r\n           \r\n          </ion-thumbnail>\r\n          <h2>{{ghn}}</h2>\r\n          <h6 style=\"color:rgb(155, 7, 69)\">Feedback</h6>\r\n          <button ion-button clear item-end >{{arrLength}}</button>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-card>\r\n   -->\r\n</ion-content>\r\n";

/***/ })

}]);
//# sourceMappingURL=src_app_dashboard_dashboard_module_ts.js.map