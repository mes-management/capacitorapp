"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_ge-allfeedbackdata_ge-allfeedbackdata_module_ts"],{

/***/ 10166:
/*!*******************************************************************************!*\
  !*** ./src/app/pages/ge-allfeedbackdata/ge-allfeedbackdata-routing.module.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeAllfeedbackdataPageRoutingModule": () => (/* binding */ GeAllfeedbackdataPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 65485);
/* harmony import */ var _ge_allfeedbackdata_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ge-allfeedbackdata.page */ 3023);




const routes = [
    {
        path: '',
        component: _ge_allfeedbackdata_page__WEBPACK_IMPORTED_MODULE_0__.GeAllfeedbackdataPage
    }
];
let GeAllfeedbackdataPageRoutingModule = class GeAllfeedbackdataPageRoutingModule {
};
GeAllfeedbackdataPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], GeAllfeedbackdataPageRoutingModule);



/***/ }),

/***/ 28423:
/*!***********************************************************************!*\
  !*** ./src/app/pages/ge-allfeedbackdata/ge-allfeedbackdata.module.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeAllfeedbackdataPageModule": () => (/* binding */ GeAllfeedbackdataPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38143);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 31777);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 95472);
/* harmony import */ var _ge_allfeedbackdata_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ge-allfeedbackdata-routing.module */ 10166);
/* harmony import */ var _ge_allfeedbackdata_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ge-allfeedbackdata.page */ 3023);







let GeAllfeedbackdataPageModule = class GeAllfeedbackdataPageModule {
};
GeAllfeedbackdataPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _ge_allfeedbackdata_routing_module__WEBPACK_IMPORTED_MODULE_0__.GeAllfeedbackdataPageRoutingModule
        ],
        declarations: [_ge_allfeedbackdata_page__WEBPACK_IMPORTED_MODULE_1__.GeAllfeedbackdataPage]
    })
], GeAllfeedbackdataPageModule);



/***/ }),

/***/ 3023:
/*!*********************************************************************!*\
  !*** ./src/app/pages/ge-allfeedbackdata/ge-allfeedbackdata.page.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeAllfeedbackdataPage": () => (/* binding */ GeAllfeedbackdataPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _ge_allfeedbackdata_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ge-allfeedbackdata.page.html?ngResource */ 70733);
/* harmony import */ var _ge_allfeedbackdata_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ge-allfeedbackdata.page.scss?ngResource */ 32771);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 51109);




let GeAllfeedbackdataPage = class GeAllfeedbackdataPage {
    constructor() { }
    ngOnInit() {
    }
};
GeAllfeedbackdataPage.ctorParameters = () => [];
GeAllfeedbackdataPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-ge-allfeedbackdata',
        template: _ge_allfeedbackdata_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_ge_allfeedbackdata_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], GeAllfeedbackdataPage);



/***/ }),

/***/ 32771:
/*!**********************************************************************************!*\
  !*** ./src/app/pages/ge-allfeedbackdata/ge-allfeedbackdata.page.scss?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJnZS1hbGxmZWVkYmFja2RhdGEucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 70733:
/*!**********************************************************************************!*\
  !*** ./src/app/pages/ge-allfeedbackdata/ge-allfeedbackdata.page.html?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>GeAllfeedbackdata</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_pages_ge-allfeedbackdata_ge-allfeedbackdata_module_ts.js.map