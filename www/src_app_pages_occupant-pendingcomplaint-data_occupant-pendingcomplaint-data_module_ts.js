"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_occupant-pendingcomplaint-data_occupant-pendingcomplaint-data_module_ts"],{

/***/ 74429:
/*!*******************************************************************************************************!*\
  !*** ./src/app/pages/occupant-pendingcomplaint-data/occupant-pendingcomplaint-data-routing.module.ts ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OccupantPendingcomplaintDataPageRoutingModule": () => (/* binding */ OccupantPendingcomplaintDataPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 65485);
/* harmony import */ var _occupant_pendingcomplaint_data_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./occupant-pendingcomplaint-data.page */ 46059);




const routes = [
    {
        path: '',
        component: _occupant_pendingcomplaint_data_page__WEBPACK_IMPORTED_MODULE_0__.OccupantPendingcomplaintDataPage
    }
];
let OccupantPendingcomplaintDataPageRoutingModule = class OccupantPendingcomplaintDataPageRoutingModule {
};
OccupantPendingcomplaintDataPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], OccupantPendingcomplaintDataPageRoutingModule);



/***/ }),

/***/ 57007:
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/occupant-pendingcomplaint-data/occupant-pendingcomplaint-data.module.ts ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OccupantPendingcomplaintDataPageModule": () => (/* binding */ OccupantPendingcomplaintDataPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38143);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 31777);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 95472);
/* harmony import */ var _occupant_pendingcomplaint_data_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./occupant-pendingcomplaint-data-routing.module */ 74429);
/* harmony import */ var _occupant_pendingcomplaint_data_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./occupant-pendingcomplaint-data.page */ 46059);







let OccupantPendingcomplaintDataPageModule = class OccupantPendingcomplaintDataPageModule {
};
OccupantPendingcomplaintDataPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _occupant_pendingcomplaint_data_routing_module__WEBPACK_IMPORTED_MODULE_0__.OccupantPendingcomplaintDataPageRoutingModule
        ],
        declarations: [_occupant_pendingcomplaint_data_page__WEBPACK_IMPORTED_MODULE_1__.OccupantPendingcomplaintDataPage]
    })
], OccupantPendingcomplaintDataPageModule);



/***/ }),

/***/ 46059:
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/occupant-pendingcomplaint-data/occupant-pendingcomplaint-data.page.ts ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OccupantPendingcomplaintDataPage": () => (/* binding */ OccupantPendingcomplaintDataPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _occupant_pendingcomplaint_data_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./occupant-pendingcomplaint-data.page.html?ngResource */ 50284);
/* harmony import */ var _occupant_pendingcomplaint_data_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./occupant-pendingcomplaint-data.page.scss?ngResource */ 99534);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 65485);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 95472);
/* harmony import */ var src_app_providers_auth_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/providers/auth-service.service */ 23539);
/* harmony import */ var src_app_providers_occupant_registration_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/providers/occupant-registration.service */ 78197);








let OccupantPendingcomplaintDataPage = class OccupantPendingcomplaintDataPage {
    constructor(OccupantService, route, loadingController) {
        this.OccupantService = OccupantService;
        this.route = route;
        this.loadingController = loadingController;
        this.regData = { Comments: '', Status: '', USR_ID: '', ComplaintCode: '' };
        this.Occupantdata = {
            OccupantName: "",
            EmailID: "",
            MobileNumber: "",
            Location: "",
            ComplaintDescription: "",
            CreatedDate: "",
            ServiceTypeName: "",
            AccomodationType: "",
            AccomodationNo: "",
            Status: "",
        };
    }
    ngOnInit() {
        let ComplaintCode = localStorage.getItem('complaintCode');
        // alert(ComplaintCode)
        this.OccupantService.GetOccupantPendingComplaintsData(ComplaintCode).subscribe(res => {
            if (res) {
                this.Occupantdata.OccupantName = res[0].name;
                // this.Occupantdata.EmailID=res[0].emailID;
                this.Occupantdata.MobileNumber = res[0].mobileNumber;
                this.Occupantdata.Location = res[0].locationName;
                this.Occupantdata.ComplaintDescription = res[0].complaintDescription;
                this.Occupantdata.CreatedDate = res[0].createdDate;
                this.Occupantdata.ServiceTypeName = res[0].serviceTypeName;
                this.Occupantdata.AccomodationType = res[0].accommodationType;
                this.Occupantdata.AccomodationNo = res[0].accommodationNumber;
                this.Occupantdata.Status = res[0].status;
                this.Occupantdata.ComplaintDescription = res[0].complaintDescription;
            }
        });
    }
};
OccupantPendingcomplaintDataPage.ctorParameters = () => [
    { type: src_app_providers_occupant_registration_service__WEBPACK_IMPORTED_MODULE_3__.OccupantRegistrationService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
OccupantPendingcomplaintDataPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-occupant-pendingcomplaint-data',
        template: _occupant_pendingcomplaint_data_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        providers: [src_app_providers_auth_service_service__WEBPACK_IMPORTED_MODULE_2__.AuthServiceService, src_app_providers_occupant_registration_service__WEBPACK_IMPORTED_MODULE_3__.OccupantRegistrationService],
        styles: [_occupant_pendingcomplaint_data_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], OccupantPendingcomplaintDataPage);



/***/ }),

/***/ 78197:
/*!************************************************************!*\
  !*** ./src/app/providers/occupant-registration.service.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OccupantRegistrationService": () => (/* binding */ OccupantRegistrationService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 78336);



let apiUrl = 'http://meswebapi.neemus.in/api/';
let OccupantRegistrationService = class OccupantRegistrationService {
    constructor(http) {
        this.http = http;
    }
    getAccomdationTypes(data) {
        return this.http.get(apiUrl + 'Accomodation/GetdistinctAccomodationType?ComplexID=' + data);
    }
    getlocationdetails(OfficeID) {
        return this.http.get(apiUrl + 'Location/GetLocationsByDivision?OfficeID=' + OfficeID);
    }
    getcomplexdetails(LocationID) {
        return this.http.get(apiUrl + 'Location/GetComplexByLocationID?LocationID=' + LocationID);
    }
    AccomdationData(ComplexID, BuildingName) {
        return this.http.get(apiUrl + 'Accomodation/GetAccomodationByComplexID?ComplexID=' + ComplexID + '&&BuildingName=' + BuildingName);
    }
    getDivisiondetails() {
        return this.http.get(apiUrl + 'Division/GetDivisions');
    }
    getSubDivisiondetails(divisionID) {
        return this.http.get(apiUrl + 'SubDivision/GetSubDivisionDetailsById?divisionid=' + divisionID);
    }
    checkMobileExists(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/checkMobileExists?MobileNumber=' + MobileNumber);
    }
    checkOccupantStatus(OccupantID) {
        return this.http.get(apiUrl + 'OccupantDetails/checkOccupantStatus?OccupantID=' + OccupantID);
    }
    AddOccupant(data) {
        //alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl + 'OccupantDetails/InsertOccupantDetails', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    GetOccupantDetailsByLocID(USR_ID) {
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByLocID?USR_ID=' + USR_ID);
    }
    ApprovedOccupantDetails(USR_ID) {
        return this.http.get(apiUrl + 'OccupantDetails/ApprovedOccupantDetails?USR_ID=' + USR_ID);
    }
    ViewApprovedOccupantDetails(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/ApprovedOccupantDetailsByMobileNumber?MobileNumber=' + MobileNumber);
    }
    AccomdationData11(ComplexID) {
        return this.http.get(apiUrl + 'Accomodation/GetAccomodationByComplexID1?ComplexID=' + ComplexID);
    }
    // AccomdationData1(): Observable<any> {  
    //     return this.http.get(apiUrl+'Accomodation/GetAccomodationDetails');  
    // }
    RejectedOccupantDetails(USR_ID) {
        return this.http.get(apiUrl + 'OccupantDetails/RejectedOccupantDetails?USR_ID=' + USR_ID);
    }
    RejectedOccupantDetailsByMono(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/RejectedOccupantDetailsByMobilenumber?MobileNumber=' + MobileNumber);
    }
    GetOccupantDetailsByOccupantID(OccupantID) {
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByOccupantID?OccupantID=' + OccupantID);
    }
    GetOccupantDetailsByMobileNumber(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByMobileNumber?MobileNumber=' + MobileNumber);
    }
    UpdateOccupantApprovalStatus(regData) {
        return this.http.get(apiUrl + 'OccupantDetails/UpdateOccupantApprovalStatus?OccupantID=' + regData.OccupantID + '&&Remarks=' + regData.Remarks + '&&Status=' + regData.Status);
    }
    //pavani 
    AccomdationData1() {
        return this.http.get(apiUrl + 'Accomodation/GetAccomodationDetails');
    }
    ServiceType() {
        return this.http.get(apiUrl + 'ServiceType/GetServiceType');
    }
    getOccupantslist() {
        //alert('hh')
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByAccommodation');
    }
    viewAllFeedbackDetails(OccupantID) {
        return this.http.get(apiUrl + 'Feedback/ViewFeedbackDetails?OccupantID=' + OccupantID);
    }
    viewOccupantFeedbackData(ComplaintCode) {
        //   alert(ComplaintCode)
        return this.http.get(apiUrl + 'Feedback/ViewFeedbackDetailsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    GetGEPendingComplaintsData(ComplaintCode) {
        // alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/GEviewpendingcomplaints?ComplaintCode=' + ComplaintCode);
    }
    GetGEViewResolvedComplaintsData(ComplaintCode) {
        //alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/ViewGEResolvedComplaintsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    GetOccupantPendingComplaintsData(ComplaintCode) {
        //alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantPendingComplaintsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    // GetOccupantFeedbackData(ComplaintCode:any):Observable<any>{
    //     //alert(ComplaintCode)
    //      return this.http.get(apiUrl+'Feedback/ViewFeedbackDetailsByComplaintCode?ComplaintCode='+ComplaintCode); 
    //  }
    GetOccupantResolvedComplaintsData(ComplaintCode) {
        //alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantResolvedComplaintsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    GetOccupantWorkInComplaintsData(ComplaintCode) {
        //alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantWorkinComplaintsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    GetGEWorkinprogressComplaintsdata(ComplaintCode) {
        // alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/GEviewworkinprogresscomplaints?ComplaintCode=' + ComplaintCode);
    }
    AddGEWorkinprogress(data) {
        //  alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl + 'ComplaintDetails/GEUpdatetworkinProgressComplaints', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    AddGeapprovedComplaints(regData) {
        //alert(JSON.stringify(data));
        //  const promise2 = this.http.post(apiUrl+'ComplaintDetails/GEUpdatetComplaints',(data)).toPromise(); 
        //  promise2.then((data)=>{
        //  }).catch((error)=>{
        //  });
        //  return promise2; 
        return this.http.get(apiUrl + 'ComplaintDetails/GEUpdatetComplaints?ComplaintCode=' + regData.ComplaintCode + '&&Comments=' + regData.Comments + '&&Status=' + regData.Status + '&&USR_ID=' + regData.USR_ID);
    }
    AddComplaint(data) {
        // alert(JSON.stringify(data));
        const promise2 = this.http.post(apiUrl + 'ComplaintDetails/InsertComplaints', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    AddFeedbackInfo(data) {
        // alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl + 'Feedback/InsertFeedback', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    GetAllPendingComplaints(USR_ID) {
        return this.http.get(apiUrl + 'ComplaintDetails/GetPendingComplaintsByLocID?USR_ID=' + USR_ID);
    }
    getComplaintFeedback(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintFeedback/GetComplaintFeedback?OccupantID=' + OccupantID);
    }
    GetSelfData(SelfOccupantID) {
        // alert(SelfOccupantID)
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByOccupantID1?OccupantID=' + SelfOccupantID);
    }
    GetOccupantPendingComplaints(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantPendingComplaints?OccupantID=' + OccupantID);
    }
    GetOccupantResolvedComplaints(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantResolvedComplaints?OccupantID=' + OccupantID);
    }
    GetOccupantWorkinprogressComplaints(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantWorkinprogressComplaints?OccupantID=' + OccupantID);
    }
    GetGEWorkinProgressComplaints(USR_ID) {
        return this.http.get(apiUrl + 'ComplaintDetails/ViewGEWorkinprogressComplaintsByLocID?USR_ID=' + USR_ID);
    }
    GetGEResolvedComplaints(USR_ID) {
        return this.http.get(apiUrl + 'ComplaintDetails/ViewGEResolvedComplaintsByLocID?USR_ID=' + USR_ID);
    }
    ViewGESideFeedback(USR_ID) {
        return this.http.get(apiUrl + 'Feedback/ViewFeedbackDetailsByUSRID?USR_ID=' + USR_ID);
    }
};
OccupantRegistrationService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpClient }
];
OccupantRegistrationService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)()
], OccupantRegistrationService);



/***/ }),

/***/ 99534:
/*!**********************************************************************************************************!*\
  !*** ./src/app/pages/occupant-pendingcomplaint-data/occupant-pendingcomplaint-data.page.scss?ngResource ***!
  \**********************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJvY2N1cGFudC1wZW5kaW5nY29tcGxhaW50LWRhdGEucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 50284:
/*!**********************************************************************************************************!*\
  !*** ./src/app/pages/occupant-pendingcomplaint-data/occupant-pendingcomplaint-data.page.html?ngResource ***!
  \**********************************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar class=\"grad\" color=\"transparent\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title> Pending Complaints</ion-title>\n  </ion-toolbar>\n\n\n<style>\n  .button-md {\n  height: 3.6rem !important;\n  font-size: 1em;\n  float: inherit;\n}\n</style>\n</ion-header>\n\n<ion-content>\n  <div>\n    <ion-item color=\"transparent\" lines=\"none\" style=\"background-color: lightgrey;\">\n      <h4 class=\"headerr\">Occupant Details:</h4>\n    </ion-item> \n    <ion-card class=\"card-diplay\"> \n      <span> Name:{{this.Occupantdata.OccupantName}} </span><br>\n      <span>Phone No:{{this.Occupantdata.MobileNumber}} </span><br>\n  \n        <span>Created Date:{{this.Occupantdata.CreatedDate}}</span><br>\n        <span>Location: {{this.Occupantdata.Location}}</span><br>\n       <span>Accomodation Type: {{this.Occupantdata.AccomodationType}}</span><br>\n        <span>Accomodation Number: {{this.Occupantdata.AccomodationNo}}</span><br><br>\n    </ion-card>\n  </div>\n  <div>\n    <ion-item color=\"transparent\" lines=\"none\" style=\"background-color: lightgrey;\">\n      <h4 class=\"headerr\">Complaint Details:</h4>\n    </ion-item> \n    <ion-card class=\"card-diplay-detail\"> \n        \n      <span>ServiceType:{{this.Occupantdata.ServiceTypeName}}</span><br>\n      <span>Description:{{this.Occupantdata.ComplaintDescription}} </span><br>\n      <span>Status:{{this.Occupantdata.Status}} </span><br>\n   \n    </ion-card>\n    \n  </div>\n\n  \n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_pages_occupant-pendingcomplaint-data_occupant-pendingcomplaint-data_module_ts.js.map