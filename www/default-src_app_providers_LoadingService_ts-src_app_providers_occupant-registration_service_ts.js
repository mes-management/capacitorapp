"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["default-src_app_providers_LoadingService_ts-src_app_providers_occupant-registration_service_ts"],{

/***/ 67145:
/*!*********************************************!*\
  !*** ./src/app/providers/LoadingService.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoadingService": () => (/* binding */ LoadingService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 95472);



let LoadingService = class LoadingService {
    constructor(loadingController) {
        this.loadingController = loadingController;
        this.isLoading = false;
    }
    present() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
            // duration: 5000,
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    dismiss() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
};
LoadingService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.LoadingController }
];
LoadingService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], LoadingService);



/***/ }),

/***/ 78197:
/*!************************************************************!*\
  !*** ./src/app/providers/occupant-registration.service.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OccupantRegistrationService": () => (/* binding */ OccupantRegistrationService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 48163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 51109);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 78336);



let apiUrl = 'http://meswebapi.neemus.in/api/';
let OccupantRegistrationService = class OccupantRegistrationService {
    constructor(http) {
        this.http = http;
    }
    getAccomdationTypes(data) {
        return this.http.get(apiUrl + 'Accomodation/GetdistinctAccomodationType?ComplexID=' + data);
    }
    getlocationdetails(OfficeID) {
        return this.http.get(apiUrl + 'Location/GetLocationsByDivision?OfficeID=' + OfficeID);
    }
    getcomplexdetails(LocationID) {
        return this.http.get(apiUrl + 'Location/GetComplexByLocationID?LocationID=' + LocationID);
    }
    AccomdationData(ComplexID, BuildingName) {
        return this.http.get(apiUrl + 'Accomodation/GetAccomodationByComplexID?ComplexID=' + ComplexID + '&&BuildingName=' + BuildingName);
    }
    getDivisiondetails() {
        return this.http.get(apiUrl + 'Division/GetDivisions');
    }
    getSubDivisiondetails(divisionID) {
        return this.http.get(apiUrl + 'SubDivision/GetSubDivisionDetailsById?divisionid=' + divisionID);
    }
    checkMobileExists(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/checkMobileExists?MobileNumber=' + MobileNumber);
    }
    checkOccupantStatus(OccupantID) {
        return this.http.get(apiUrl + 'OccupantDetails/checkOccupantStatus?OccupantID=' + OccupantID);
    }
    AddOccupant(data) {
        //alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl + 'OccupantDetails/InsertOccupantDetails', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    GetOccupantDetailsByLocID(USR_ID) {
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByLocID?USR_ID=' + USR_ID);
    }
    ApprovedOccupantDetails(USR_ID) {
        return this.http.get(apiUrl + 'OccupantDetails/ApprovedOccupantDetails?USR_ID=' + USR_ID);
    }
    ViewApprovedOccupantDetails(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/ApprovedOccupantDetailsByMobileNumber?MobileNumber=' + MobileNumber);
    }
    AccomdationData11(ComplexID) {
        return this.http.get(apiUrl + 'Accomodation/GetAccomodationByComplexID1?ComplexID=' + ComplexID);
    }
    // AccomdationData1(): Observable<any> {  
    //     return this.http.get(apiUrl+'Accomodation/GetAccomodationDetails');  
    // }
    RejectedOccupantDetails(USR_ID) {
        return this.http.get(apiUrl + 'OccupantDetails/RejectedOccupantDetails?USR_ID=' + USR_ID);
    }
    RejectedOccupantDetailsByMono(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/RejectedOccupantDetailsByMobilenumber?MobileNumber=' + MobileNumber);
    }
    GetOccupantDetailsByOccupantID(OccupantID) {
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByOccupantID?OccupantID=' + OccupantID);
    }
    GetOccupantDetailsByMobileNumber(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByMobileNumber?MobileNumber=' + MobileNumber);
    }
    UpdateOccupantApprovalStatus(regData) {
        return this.http.get(apiUrl + 'OccupantDetails/UpdateOccupantApprovalStatus?OccupantID=' + regData.OccupantID + '&&Remarks=' + regData.Remarks + '&&Status=' + regData.Status);
    }
    //pavani 
    AccomdationData1() {
        return this.http.get(apiUrl + 'Accomodation/GetAccomodationDetails');
    }
    ServiceType() {
        return this.http.get(apiUrl + 'ServiceType/GetServiceType');
    }
    getOccupantslist() {
        //alert('hh')
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByAccommodation');
    }
    viewAllFeedbackDetails(OccupantID) {
        return this.http.get(apiUrl + 'Feedback/ViewFeedbackDetails?OccupantID=' + OccupantID);
    }
    viewOccupantFeedbackData(ComplaintCode) {
        //   alert(ComplaintCode)
        return this.http.get(apiUrl + 'Feedback/ViewFeedbackDetailsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    GetGEPendingComplaintsData(ComplaintCode) {
        // alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/GEviewpendingcomplaints?ComplaintCode=' + ComplaintCode);
    }
    GetGEViewResolvedComplaintsData(ComplaintCode) {
        //alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/ViewGEResolvedComplaintsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    GetOccupantPendingComplaintsData(ComplaintCode) {
        //alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantPendingComplaintsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    // GetOccupantFeedbackData(ComplaintCode:any):Observable<any>{
    //     //alert(ComplaintCode)
    //      return this.http.get(apiUrl+'Feedback/ViewFeedbackDetailsByComplaintCode?ComplaintCode='+ComplaintCode); 
    //  }
    GetOccupantResolvedComplaintsData(ComplaintCode) {
        //alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantResolvedComplaintsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    GetOccupantWorkInComplaintsData(ComplaintCode) {
        //alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantWorkinComplaintsByComplaintCode?ComplaintCode=' + ComplaintCode);
    }
    GetGEWorkinprogressComplaintsdata(ComplaintCode) {
        // alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/GEviewworkinprogresscomplaints?ComplaintCode=' + ComplaintCode);
    }
    AddGEWorkinprogress(data) {
        //  alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl + 'ComplaintDetails/GEUpdatetworkinProgressComplaints', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    AddGeapprovedComplaints(regData) {
        //alert(JSON.stringify(data));
        //  const promise2 = this.http.post(apiUrl+'ComplaintDetails/GEUpdatetComplaints',(data)).toPromise(); 
        //  promise2.then((data)=>{
        //  }).catch((error)=>{
        //  });
        //  return promise2; 
        return this.http.get(apiUrl + 'ComplaintDetails/GEUpdatetComplaints?ComplaintCode=' + regData.ComplaintCode + '&&Comments=' + regData.Comments + '&&Status=' + regData.Status + '&&USR_ID=' + regData.USR_ID);
    }
    AddComplaint(data) {
        // alert(JSON.stringify(data));
        const promise2 = this.http.post(apiUrl + 'ComplaintDetails/InsertComplaints', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    AddFeedbackInfo(data) {
        // alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl + 'Feedback/InsertFeedback', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    GetAllPendingComplaints(USR_ID) {
        return this.http.get(apiUrl + 'ComplaintDetails/GetPendingComplaintsByLocID?USR_ID=' + USR_ID);
    }
    getComplaintFeedback(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintFeedback/GetComplaintFeedback?OccupantID=' + OccupantID);
    }
    GetSelfData(SelfOccupantID) {
        // alert(SelfOccupantID)
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByOccupantID1?OccupantID=' + SelfOccupantID);
    }
    GetOccupantPendingComplaints(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantPendingComplaints?OccupantID=' + OccupantID);
    }
    GetOccupantResolvedComplaints(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantResolvedComplaints?OccupantID=' + OccupantID);
    }
    GetOccupantWorkinprogressComplaints(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantWorkinprogressComplaints?OccupantID=' + OccupantID);
    }
    GetGEWorkinProgressComplaints(USR_ID) {
        return this.http.get(apiUrl + 'ComplaintDetails/ViewGEWorkinprogressComplaintsByLocID?USR_ID=' + USR_ID);
    }
    GetGEResolvedComplaints(USR_ID) {
        return this.http.get(apiUrl + 'ComplaintDetails/ViewGEResolvedComplaintsByLocID?USR_ID=' + USR_ID);
    }
    ViewGESideFeedback(USR_ID) {
        return this.http.get(apiUrl + 'Feedback/ViewFeedbackDetailsByUSRID?USR_ID=' + USR_ID);
    }
};
OccupantRegistrationService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpClient }
];
OccupantRegistrationService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)()
], OccupantRegistrationService);



/***/ })

}]);
//# sourceMappingURL=default-src_app_providers_LoadingService_ts-src_app_providers_occupant-registration_service_ts.js.map