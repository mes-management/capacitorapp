"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["default-src_app_providers_auth-service_service_ts-src_app_providers_occupant-registration_ser-7cbe22"],{

/***/ 3539:
/*!***************************************************!*\
  !*** ./src/app/providers/auth-service.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthServiceService": () => (/* binding */ AuthServiceService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 8163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 1109);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ 1689);
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ 70);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 8336);





//import {  ServiceModel} from '../app/complaint-registration/DepartmentModel';  
let apiUrl = 'http://meswebapi.neemus.in/api/';
let AuthServiceService = class AuthServiceService {
    constructor(http) {
        this.http = http;
    }
    // getService(): Observable<any> {  
    //   return this.http.get(apiUrl+'ServiceType/GetServiceType').pipe(      
    //     map((results:any) =>results )  
    //   ); 
    //   }
    //     return this.http.get(apiUrl+'ServiceType/GetServiceType').map(results => results.json())  
    //         .catch(this.handleError);  
    // }
    /* Generated Method for the Login provider. */
    login(credentials) {
        return new Promise((resolve, reject) => {
            const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpHeaders({ 'Content-Type': 'application/json' });
            // headers.append('Content-Type', 'application/json');
            //alert(credentials)
            this.http.post(apiUrl + 'Login/LoginDetails', (credentials), { headers })
                .subscribe((res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });
    }
    get() {
        return new Promise((resolve, reject) => {
            const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpHeaders({ 'Content-Type': 'application/json' });
            headers.append('Content-Type', 'application/json');
            this.http.get(apiUrl + 'ServiceType/GetServiceType')
                .subscribe((res) => {
                resolve(res);
                // alert(apiUrl)
            }, (err) => {
                reject(err);
            });
        });
    }
    //Complaint Registration 
    complaintregister(data) {
        //alert(data)
        return new Promise((resolve, reject) => {
            const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpHeaders({ 'Content-Type': 'application/json' });
            headers.append('Content-Type', 'text/plain');
            this.http.post(apiUrl + 'Complaint/InsertComplaintDetails', (data), { headers })
                .subscribe(res => {
                (resolve(res));
            }, (err) => {
                reject(err);
            });
        });
    }
    handleError(error) {
        let errMsg;
        if (error instanceof _angular_http__WEBPACK_IMPORTED_MODULE_1__.Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return rxjs__WEBPACK_IMPORTED_MODULE_2__.Observable["throw"](errMsg);
    }
};
AuthServiceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpClient }
];
AuthServiceService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], AuthServiceService);



/***/ }),

/***/ 8197:
/*!************************************************************!*\
  !*** ./src/app/providers/occupant-registration.service.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OccupantRegistrationService": () => (/* binding */ OccupantRegistrationService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 8163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 1109);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 8336);



let apiUrl = 'http://meswebapi.neemus.in/api/';
let OccupantRegistrationService = class OccupantRegistrationService {
    constructor(http) {
        this.http = http;
    }
    getAccomdationTypes(data) {
        return this.http.get(apiUrl + 'Accomodation/GetdistinctAccomodationType?ComplexID=' + data);
    }
    getlocationdetails(OfficeID) {
        return this.http.get(apiUrl + 'Location/GetLocationsByDivision?OfficeID=' + OfficeID);
    }
    getcomplexdetails(LocationID) {
        return this.http.get(apiUrl + 'Location/GetComplexByLocationID?LocationID=' + LocationID);
    }
    AccomdationData(ComplexID, BuildingName) {
        return this.http.get(apiUrl + 'Accomodation/GetAccomodationByComplexID?ComplexID=' + ComplexID + '&&BuildingName=' + BuildingName);
    }
    getDivisiondetails() {
        return this.http.get(apiUrl + 'Division/GetDivisions');
    }
    getSubDivisiondetails(divisionID) {
        return this.http.get(apiUrl + 'SubDivision/GetSubDivisionDetailsById?divisionid=' + divisionID);
    }
    checkMobileExists(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/checkMobileExists?MobileNumber=' + MobileNumber);
    }
    checkOccupantStatus(OccupantID) {
        return this.http.get(apiUrl + 'OccupantDetails/checkOccupantStatus?OccupantID=' + OccupantID);
    }
    AddOccupant(data) {
        //alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl + 'OccupantDetails/InsertOccupantDetails', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    GetOccupantDetailsByLocID(USR_ID) {
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByLocID?USR_ID=' + USR_ID);
    }
    ApprovedOccupantDetails(USR_ID) {
        return this.http.get(apiUrl + 'OccupantDetails/ApprovedOccupantDetails?USR_ID=' + USR_ID);
    }
    ViewApprovedOccupantDetails(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/ApprovedOccupantDetailsByMobileNumber?MobileNumber=' + MobileNumber);
    }
    RejectedOccupantDetails(USR_ID) {
        return this.http.get(apiUrl + 'OccupantDetails/RejectedOccupantDetails?USR_ID=' + USR_ID);
    }
    GetOccupantDetailsByOccupantID(OccupantID) {
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByOccupantID?OccupantID=' + OccupantID);
    }
    GetOccupantDetailsByMobileNumber(MobileNumber) {
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByMobileNumber?MobileNumber=' + MobileNumber);
    }
    UpdateOccupantApprovalStatus(regData) {
        return this.http.get(apiUrl + 'OccupantDetails/UpdateOccupantApprovalStatus?OccupantID=' + regData.OccupantID + '&&Remarks=' + regData.Remarks + '&&Status=' + regData.Status);
    }
    //pavani 
    AccomdationData1() {
        return this.http.get(apiUrl + 'Accomodation/GetAccomodationDetails');
    }
    ServiceType() {
        return this.http.get(apiUrl + 'ServiceType/GetServiceType');
    }
    getOccupantslist() {
        //alert('hh')
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByAccommodation');
    }
    viewAllFeedbackDetails(OccupantID) {
        return this.http.get(apiUrl + 'Feedback/ViewFeedbackDetails?OccupantID=' + OccupantID);
    }
    GetGEPendingComplaintsData(ComplaintCode) {
        // alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/GEviewpendingcomplaints?ComplaintCode=' + ComplaintCode);
    }
    GetGEWorkinprogressComplaintsdata(ComplaintCode) {
        // alert(ComplaintCode)
        return this.http.get(apiUrl + 'ComplaintDetails/GEviewworkinprogresscomplaints?ComplaintCode=' + ComplaintCode);
    }
    AddGEWorkinprogress(data) {
        //  alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl + 'ComplaintDetails/GEUpdatetworkinProgressComplaints', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    AddGeapprovedComplaints(regData) {
        //alert(JSON.stringify(data));
        //  const promise2 = this.http.post(apiUrl+'ComplaintDetails/GEUpdatetComplaints',(data)).toPromise(); 
        //  promise2.then((data)=>{
        //  }).catch((error)=>{
        //  });
        //  return promise2; 
        return this.http.get(apiUrl + 'ComplaintDetails/GEUpdatetComplaints?ComplaintCode=' + regData.ComplaintCode + '&&Comments=' + regData.Comments + '&&Status=' + regData.Status + '&&USR_ID=' + regData.USR_ID);
    }
    AddComplaint(data) {
        // alert(JSON.stringify(data));
        const promise2 = this.http.post(apiUrl + 'ComplaintDetails/InsertComplaints', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    AddFeedbackInfo(data) {
        // alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl + 'Feedback/InsertFeedback', (data)).toPromise();
        promise2.then((data) => {
        }).catch((error) => {
        });
        return promise2;
    }
    GetAllPendingComplaints(USR_ID) {
        return this.http.get(apiUrl + 'ComplaintDetails/GetPendingComplaintsByLocID?USR_ID=' + USR_ID);
    }
    getComplaintFeedback(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintFeedback/GetComplaintFeedback?OccupantID=' + OccupantID);
    }
    GetSelfData(SelfOccupantID) {
        // alert(SelfOccupantID)
        return this.http.get(apiUrl + 'OccupantDetails/GetOccupantDetailsByOccupantID1?OccupantID=' + SelfOccupantID);
    }
    GetOccupantPendingComplaints(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantPendingComplaints?OccupantID=' + OccupantID);
    }
    GetOccupantResolvedComplaints(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantResolvedComplaints?OccupantID=' + OccupantID);
    }
    GetOccupantWorkinprogressComplaints(OccupantID) {
        return this.http.get(apiUrl + 'ComplaintDetails/OccupantWorkinprogressComplaints?OccupantID=' + OccupantID);
    }
    GetGEWorkinProgressComplaints(USR_ID) {
        return this.http.get(apiUrl + 'ComplaintDetails/ViewGEWorkinprogressComplaintsByLocID?USR_ID=' + USR_ID);
    }
    GetGEResolvedComplaints(USR_ID) {
        return this.http.get(apiUrl + 'ComplaintDetails/ViewGEResolvedComplaintsByLocID?USR_ID=' + USR_ID);
    }
    ViewGESideFeedback(USR_ID) {
        return this.http.get(apiUrl + 'Feedback/ViewFeedbackDetailsByUSRID?USR_ID=' + USR_ID);
    }
};
OccupantRegistrationService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpClient }
];
OccupantRegistrationService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)()
], OccupantRegistrationService);



/***/ })

}]);
//# sourceMappingURL=default-src_app_providers_auth-service_service_ts-src_app_providers_occupant-registration_ser-7cbe22.js.map