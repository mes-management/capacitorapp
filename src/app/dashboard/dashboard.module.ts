import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardPage,
  },

];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule
  ],
  declarations: [DashboardPage]
})

@NgModule({
  declarations: [
    DashboardPage
    ,
  ],
  imports: [
    RouterModule.forChild(
    routes
  ),
  // SharedStaticModule,
  //   IonicPageModule.forChild(DashboardPage),
  ],
})
export class DashboardPageModule {}

