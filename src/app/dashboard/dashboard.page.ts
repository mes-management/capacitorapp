import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';  
import { Router } from '@angular/router';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  count:any;
  Pendingcount:any;
  Resolvedcount:any;
  arrLength:any;
  locations:any;
  locationData={LocationID:''};
  gaming:any;
  data:any;
  constructor( public http:Http,private route :Router, ) 
  {

    this.http.get('http://meswebapi.neemus.in/api/Location/GetLocationDetails').pipe(map(res => res.json())).subscribe(data => {
      console.log(data);
      alert(JSON.stringify(data))
      this.locations=data;
      //data[0].acc_DetailsID;
      //this.optionsList=data;
    }
    );
   // alert("dghfgh")
    // this.http.get('http://meswebapi.neemus.in/api/Complaint/GetRegisteredComplaintDetails').map(res => res.json()).subscribe(data => {
    //   console.log(data);
    //   //alert(JSON.stringify(data[0].acc_DetailsID))
    //   this.count=data[0].acc_DetailsID;
      //this.optionsList=data;
    //}
    //);
   }
//event: Event

RegisteredView(){
  this.route.navigate(['/viewregisteredcom']);
}

PendingView(){
  this.route.navigate(['/viewgependingcomplaints']);
}

  onSelectChangeAccom(data :any){
    
    this.locationData.LocationID=data.locationID;
    //alert(JSON.stringify(this.locationData))
 
   this.http.post('http://meswebapi.neemus.in/api/Complaint/GetRegisteredComplaintDetails',(this.locationData)).pipe(map(res => res.json())).subscribe(data => {
     this.count=data[0].acc_DetailsID;
    
    }, (err) => {
      //reject(err);
    });

    //Pending Complaints
    this.http.post('http://meswebapi.neemus.in/api/Complaint/GetPendingComplaintDetails',(this.locationData)).pipe(map(res => res.json())).subscribe(data => {
      this.Pendingcount=data[0].acc_DetailsID;
     
     }, (err) => {
       //reject(err);
     });

      //Resolved Complaints
    this.http.post('http://meswebapi.neemus.in/api/Complaint/GetResolvedComplaintDetails',(this.locationData)).pipe(map(res => res.json())).subscribe(data => {
      this.Resolvedcount=data[0].acc_DetailsID;
     
     }, (err) => {
       //reject(err);
     });
 }
// optionsFn()
// {
//   alert(this.gaming)
// }
// changelocation(datas)
// {
//   alert(JSON.stringify(datas))
//   this.locationData.LocationID=datas;
//   alert(JSON.stringify(this.locationData))
// }
  ngOnInit() {
  }

}
