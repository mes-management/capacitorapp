// import { NgModule } from '@angular/core';
// import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';



const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login-user',
    pathMatch: 'full'
  },
  {
    path: 'complaintregistration',
    loadChildren: () => import('./pages/complaint-registration/complaint-registration.module').then( m => m.ComplaintRegistrationPageModule)
  },
  {
    path: 'view-complaints',
    loadChildren: () => import('./pages/view-complaints/view-complaints.module').then( m => m.ViewComplaintsPageModule)
  },
  {
    path: 'feedback',
    loadChildren: () => import('./pages/feedback/feedback.module').then( m => m.FeedbackPageModule)
  },
  {
    path: 'give-feedback',
    loadChildren: () => import('./pages/give-feedback/give-feedback.module').then( m => m.GiveFeedbackPageModule)
  },
  {
    path: 'view-feedback-details',
    loadChildren: () => import('./pages/view-feedback-details/view-feedback-details.module').then( m => m.ViewFeedbackDetailsPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'login-user',
    loadChildren: () => import('./login-user/login-user.module').then( m => m.LoginUserPageModule)
  },
  {
    path: 'viewregisteredcom',
    loadChildren: () => import('./viewregisteredcom/viewregisteredcom.module').then( m => m.ViewregisteredcomPageModule)
  },
  {
    path: 'viewgependingcomplaints',
    loadChildren: () => import('./viewgependingcomplaints/viewgependingcomplaints.module').then( m => m.ViewgependingcomplaintsPageModule)
  },
  {
    path: 'occupantregistration',
    loadChildren: () => import('./pages/occupantregistration/occupantregistration.module').then( m => m.OccupantregistrationPageModule)
  },
  {
    path: 'occupantregdetails',
    loadChildren: () => import('./pages/occupantregdetails/occupantregdetails.module').then( m => m.OccupantregdetailsPageModule)
  },
  
  {
  path: 'admin-view-occupant-data',
  loadChildren: () => import('./pages/admin-view-occupant-data/admin-view-occupant-data.module').then( m => m.AdminViewOccupantDataPageModule)
},
{
  path: 'admin-approve-occupant',
  loadChildren: () => import('./pages/admin-approve-occupant/admin-approve-occupant.module').then( m => m.AdminApproveOccupantPageModule)
},
{
  path: 'view-approved-occupants',
  loadChildren: () => import('./pages/view-approved-occupants/view-approved-occupants.module').then( m => m.ViewApprovedOccupantsPageModule)
},
{
  path: 'view-occupant-details',
  loadChildren: () => import('./pages/view-occupant-details/view-occupant-details.module').then( m => m.ViewOccupantDetailsPageModule)
},
{
  path: 'view-rejected-occupants',
  loadChildren: () => import('./pages/view-rejected-occupants/view-rejected-occupants.module').then( m => m.ViewRejectedOccupantsPageModule)
},
{
  path: 'viewgependingcomplaints',
  loadChildren: () => import('./viewgependingcomplaints/viewgependingcomplaints.module').then( m => m.ViewgependingcomplaintsPageModule)
},
{
  path: 'geview-pending-complaints',
  loadChildren: () => import('./pages/geview-pending-complaints/geview-pending-complaints.module').then( m => m.GEViewPendingComplaintsPageModule)
},


{
  path: 'view-occupant-pending-complaints',
  loadChildren: () => import('./pages/view-occupant-pending-complaints/view-occupant-pending-complaints.module').then( m => m.ViewOccupantPendingComplaintsPageModule)
},
{
  path: 'view-occupant-resolved-complaints',
  loadChildren: () => import('./pages/view-occupant-resolved-complaints/view-occupant-resolved-complaints.module').then( m => m.ViewOccupantResolvedComplaintsPageModule)
},
{
  path: 'view-occupant-workinprogress-complaints',
  loadChildren: () => import('./pages/view-occupant-workinprogress-complaints/view-occupant-workinprogress-complaints.module').then( m => m.ViewOccupantWorkinprogressComplaintsPageModule)
},
{
  path: 'view-geworkinprogress-complaints',
  loadChildren: () => import('./pages/view-geworkinprogress-complaints/view-geworkinprogress-complaints.module').then( m => m.ViewGEWorkinprogressComplaintsPageModule)
},
{
  path: 'view-geresolved-complaints',
  loadChildren: () => import('./pages/view-geresolved-complaints/view-geresolved-complaints.module').then( m => m.ViewGEResolvedComplaintsPageModule)
},
  {
    path: 'geworking-progress-complaints',
    loadChildren: () => import('./pages/geworking-progress-complaints/geworking-progress-complaints.module').then( m => m.GEWorkingProgressComplaintsPageModule)
  },
  {
    path: 'my-profile',
    loadChildren: () => import('./pages/my-profile/my-profile.module').then( m => m.MyProfilePageModule)
  },
  {
    path: 'geside-feedback-details',
    loadChildren: () => import('./pages/geside-feedback-details/geside-feedback-details.module').then( m => m.GESideFeedbackDetailsPageModule)
  },
  {
    path: 'view-approved-data',
    loadChildren: () => import('./pages/view-approved-data/view-approved-data.module').then( m => m.ViewApprovedDataPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./pages/notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'ge-rejected-occupants',
    loadChildren: () => import('./pages/ge-rejected-occupants/ge-rejected-occupants.module').then( m => m.GeRejectedOccupantsPageModule)
  },
  {
    path: 'gefeedback-data',
    loadChildren: () => import('./pages/gefeedback-data/gefeedback-data.module').then( m => m.GEFeedbackDataPageModule)
  },
  {
    path: 'ge-allfeedbackdata',
    loadChildren: () => import('./pages/ge-allfeedbackdata/ge-allfeedbackdata.module').then( m => m.GeAllfeedbackdataPageModule)
  },
  
  {
    path: 'ge-resolved-complaint-data',
    loadChildren: () => import('./pages/ge-resolved-complaint-data/ge-resolved-complaint-data.module').then( m => m.GeResolvedComplaintDataPageModule)
  },  {
    path: 'occupant-pendingcomplaint-data',
    loadChildren: () => import('./pages/occupant-pendingcomplaint-data/occupant-pendingcomplaint-data.module').then( m => m.OccupantPendingcomplaintDataPageModule)
  },
  {
    path: 'occupant-resolved-complaint-data',
    loadChildren: () => import('./pages/occupant-resolved-complaint-data/occupant-resolved-complaint-data.module').then( m => m.OccupantResolvedComplaintDataPageModule)
  },
  {
    path: 'occupantworkin-complaint-data',
    loadChildren: () => import('./pages/occupantworkin-complaint-data/occupantworkin-complaint-data.module').then( m => m.OccupantworkinComplaintDataPageModule)
  },
  {
    path: 'occupant-feedback-data',
    loadChildren: () => import('./pages/occupant-feedback-data/occupant-feedback-data.module').then( m => m.OccupantFeedbackDataPageModule)
  },
<<<<<<< HEAD
=======
  {
    path: 'occupantregistration',
    loadChildren: () => import('./pages/occupantregistration/occupantregistration.module').then( m => m.OccupantregistrationPageModule)
  },
  {
    path: 'occupantregdetails',
    loadChildren: () => import('./pages/occupantregdetails/occupantregdetails.module').then( m => m.OccupantregdetailsPageModule)
  },
  
  {
  path: 'admin-view-occupant-data',
  loadChildren: () => import('./pages/admin-view-occupant-data/admin-view-occupant-data.module').then( m => m.AdminViewOccupantDataPageModule)
},
{
  path: 'admin-approve-occupant',
  loadChildren: () => import('./pages/admin-approve-occupant/admin-approve-occupant.module').then( m => m.AdminApproveOccupantPageModule)
},
{
  path: 'view-approved-occupants',
  loadChildren: () => import('./pages/view-approved-occupants/view-approved-occupants.module').then( m => m.ViewApprovedOccupantsPageModule)
},
{
  path: 'view-occupant-details',
  loadChildren: () => import('./pages/view-occupant-details/view-occupant-details.module').then( m => m.ViewOccupantDetailsPageModule)
},
{
  path: 'view-rejected-occupants',
  loadChildren: () => import('./pages/view-rejected-occupants/view-rejected-occupants.module').then( m => m.ViewRejectedOccupantsPageModule)
},
{
  path: 'viewgependingcomplaints',
  loadChildren: () => import('./viewgependingcomplaints/viewgependingcomplaints.module').then( m => m.ViewgependingcomplaintsPageModule)
},
{
  path: 'geview-pending-complaints',
  loadChildren: () => import('./pages/geview-pending-complaints/geview-pending-complaints.module').then( m => m.GEViewPendingComplaintsPageModule)
},


{
  path: 'view-occupant-pending-complaints',
  loadChildren: () => import('./pages/view-occupant-pending-complaints/view-occupant-pending-complaints.module').then( m => m.ViewOccupantPendingComplaintsPageModule)
},
{
  path: 'view-occupant-resolved-complaints',
  loadChildren: () => import('./pages/view-occupant-resolved-complaints/view-occupant-resolved-complaints.module').then( m => m.ViewOccupantResolvedComplaintsPageModule)
},
{
  path: 'view-occupant-workinprogress-complaints',
  loadChildren: () => import('./pages/view-occupant-workinprogress-complaints/view-occupant-workinprogress-complaints.module').then( m => m.ViewOccupantWorkinprogressComplaintsPageModule)
},
{
  path: 'view-geworkinprogress-complaints',
  loadChildren: () => import('./pages/view-geworkinprogress-complaints/view-geworkinprogress-complaints.module').then( m => m.ViewGEWorkinprogressComplaintsPageModule)
},
{
  path: 'view-geresolved-complaints',
  loadChildren: () => import('./pages/view-geresolved-complaints/view-geresolved-complaints.module').then( m => m.ViewGEResolvedComplaintsPageModule)
},
  {
    path: 'geworking-progress-complaints',
    loadChildren: () => import('./pages/geworking-progress-complaints/geworking-progress-complaints.module').then( m => m.GEWorkingProgressComplaintsPageModule)
  },
  {
    path: 'my-profile',
    loadChildren: () => import('./pages/my-profile/my-profile.module').then( m => m.MyProfilePageModule)
  },
  {
    path: 'geside-feedback-details',
    loadChildren: () => import('./pages/geside-feedback-details/geside-feedback-details.module').then( m => m.GESideFeedbackDetailsPageModule)
  },
  {
    path: 'view-approved-data',
    loadChildren: () => import('./pages/view-approved-data/view-approved-data.module').then( m => m.ViewApprovedDataPageModule)
  },

  // {
  //   path: 'view-gefeedback',
  //   loadChildren: () => import('./pages/view-gefeedback/view-gefeedback.module').then( m => m.ViewGEFeedbackPageModule)
  // },
  // {
  //   path: 'view-approved-occupantsexpage',
  //   loadChildren: () => import('./pages/view-approved-occupantsexpage/view-approved-occupantsexpage.module').then( m => m.ViewApprovedOccupantsexpagePageModule)
  // },



];
>>>>>>> 433948a77c4b4afad20f042cd316eb052ae61df0



  // {
  //   path: 'view-gefeedback',
  //   loadChildren: () => import('./pages/view-gefeedback/view-gefeedback.module').then( m => m.ViewGEFeedbackPageModule)
  // },
  // {
  //   path: 'view-approved-occupantsexpage',
  //   loadChildren: () => import('./pages/view-approved-occupantsexpage/view-approved-occupantsexpage.module').then( m => m.ViewApprovedOccupantsexpagePageModule)
  // },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppRoutingModule { }
