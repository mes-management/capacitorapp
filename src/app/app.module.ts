import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
//import { HTTP } from '@ionic-native/http/ngx';
import { HttpModule } from '@angular/http';
//import {StarRatingModule } from 'ionic4-star-rating';
//import { IonicRatingModule } from 'ionic5-rating';

//import{StarRatingModule} from 'ionic5-star-rating';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
<<<<<<< HEAD
=======

import { InterceptorService } from './interceptor.service';
>>>>>>> 433948a77c4b4afad20f042cd316eb052ae61df0

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,HttpModule,
  
     AppRoutingModule, 
     BrowserModule,
     CommonModule,
    
     AppRoutingModule,
     HttpClientModule, 
     ReactiveFormsModule,
     FormsModule,
     NgxDatatableModule,
     IonicModule.forRoot(), 
     ServiceWorkerModule.register('ngsw-worker.js', {
  enabled: environment.production,
  // Register the ServiceWorker as soon as the app is stable
  // or after 30 seconds (whichever comes first).
  registrationStrategy: 'registerWhenStable:30000'
})],
  //providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },AuthServiceService],
  providers: [
    {
     provide: HTTP_INTERCEPTORS,
     useClass: InterceptorService,
     multi: true
    }
   ],
  bootstrap: [AppComponent],
})
export class AppModule {}
