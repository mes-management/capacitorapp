import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewFeedbackDetailsPageRoutingModule } from './view-feedback-details-routing.module';

import { ViewFeedbackDetailsPage } from './view-feedback-details.page';
import { HttpClientModule } from '@angular/common/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewFeedbackDetailsPageRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxDatatableModule
  ],
  declarations: [ViewFeedbackDetailsPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ViewFeedbackDetailsPageModule {}
