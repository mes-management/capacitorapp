import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewFeedbackDetailsPage } from './view-feedback-details.page';

const routes: Routes = [
  {
    path: '',
    component: ViewFeedbackDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewFeedbackDetailsPageRoutingModule {}
