import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ViewEncapsulation } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { Style } from '@capacitor/status-bar';

@Component({
 
  selector: 'app-view-feedback-details',
  templateUrl: './view-feedback-details.page.html',
  styleUrls: ['./view-feedback-details.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService],
 
  encapsulation: ViewEncapsulation.None
})
export class ViewFeedbackDetailsPage  {
  
  //public data: Data;
  public columns: any;
  public rows: any;
  constructor( private http: HttpClient,public OccupantService:OccupantRegistrationService,private route :Router) { 
  //Style.Dark
  
    this.columns = [
      { name: 'complaintCode' },
      { name: 'occupantRemarks' },
      { name: 'occupantRating' }
    ];
    // this.OccupantService.viewAllFeedbackDetails().subscribe(res =>
     
    //   {
    //     if(res){
    //     this.rows=res
    //     //  alert(JSON.stringify(this.rows));
    //     }
    //   });
     
  }

  ngOnInit() {
    let OccupantID=localStorage.getItem('occupantID');
    this.OccupantService.viewAllFeedbackDetails(OccupantID).subscribe(res =>
     
     {
       this.rows=res;
       if(res){
        // this.rows = res[0]
        // alert(JSON.stringify(this.rows));
       }
     });
    
      
      
    }
  onActivate(event) {
    let ComplaintCode=event.row.complaintCode
    localStorage.setItem('complaintCode',ComplaintCode)
    this.route.navigateByUrl('occupant-feedback-data')
  }
}

