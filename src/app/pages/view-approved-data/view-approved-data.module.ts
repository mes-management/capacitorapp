import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewApprovedDataPageRoutingModule } from './view-approved-data-routing.module';

import { ViewApprovedDataPage } from './view-approved-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewApprovedDataPageRoutingModule
  ],
  declarations: [ViewApprovedDataPage]
})
export class ViewApprovedDataPageModule {}
