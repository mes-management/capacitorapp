import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewApprovedDataPage } from './view-approved-data.page';

const routes: Routes = [
  {
    path: '',
    component: ViewApprovedDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewApprovedDataPageRoutingModule {}
