import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewRejectedOccupantsPage } from './view-rejected-occupants.page';

const routes: Routes = [
  {
    path: '',
    component: ViewRejectedOccupantsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewRejectedOccupantsPageRoutingModule {}
