import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Data, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { LoadingService } from 'src/app/providers/LoadingService';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-view-rejected-occupants',
  templateUrl: './view-rejected-occupants.page.html',
  styleUrls: ['./view-rejected-occupants.page.scss'],
  providers:[OccupantRegistrationService],
  encapsulation: ViewEncapsulation.None
})
export class ViewRejectedOccupantsPage implements OnInit {

 
  public columns: any;
  public rows: any;
  errorMessage:any;
  constructor(private http: HttpClient,public OccupantService:OccupantRegistrationService,private route :Router,public loading: LoadingService) {
    this.columns = [
    
      { name: 'name' },
      { name: 'mobileNumber' },
      // { name: 'remarks' },
      // { name: 'createdDate' },
      { name: 'status' },
     
    ];
   
   
  }
  ngOnInit() {
    let USR_ID=localStorage.getItem('usR_ID');
    this.loading.present();
    this.OccupantService.RejectedOccupantDetails(USR_ID).subscribe(res =>
      {
       
        this.rows=res;
        this.loading.dismiss();
    }, (err) => {
      this.loading.dismiss();
    }); 
  }
  
  onActivate(event) {
    if(event.type == 'click') {
      //  let OccupantID=event.row.occupant;
      //  localStorage.setItem('OccupantID',OccupantID);
       let MobileNumber=event.row.mobileNumber;
       localStorage.setItem('mobileNumber',MobileNumber);
     
       this.route.navigateByUrl('ge-rejected-occupants');
    }
  }
}
