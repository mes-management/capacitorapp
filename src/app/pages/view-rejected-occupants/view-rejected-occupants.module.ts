import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewRejectedOccupantsPageRoutingModule } from './view-rejected-occupants-routing.module';

import { ViewRejectedOccupantsPage } from './view-rejected-occupants.page';
import { HttpClientModule } from '@angular/common/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxDatatableModule,
    ViewRejectedOccupantsPageRoutingModule
  ],
  declarations: [ViewRejectedOccupantsPage]
})
export class ViewRejectedOccupantsPageModule {}
