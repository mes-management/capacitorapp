import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewRejectedOccupantsPage } from './view-rejected-occupants.page';

describe('ViewRejectedOccupantsPage', () => {
  let component: ViewRejectedOccupantsPage;
  let fixture: ComponentFixture<ViewRejectedOccupantsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRejectedOccupantsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewRejectedOccupantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
