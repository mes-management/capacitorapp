import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GEFeedbackDataPage } from './gefeedback-data.page';

const routes: Routes = [
  {
    path: '',
    component: GEFeedbackDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GEFeedbackDataPageRoutingModule {}
