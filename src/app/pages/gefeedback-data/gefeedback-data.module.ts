import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GEFeedbackDataPageRoutingModule } from './gefeedback-data-routing.module';

import { GEFeedbackDataPage } from './gefeedback-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GEFeedbackDataPageRoutingModule
  ],
  declarations: [GEFeedbackDataPage]
})
export class GEFeedbackDataPageModule {}
