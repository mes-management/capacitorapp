import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OccupantPendingcomplaintDataPage } from './occupant-pendingcomplaint-data.page';

const routes: Routes = [
  {
    path: '',
    component: OccupantPendingcomplaintDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OccupantPendingcomplaintDataPageRoutingModule {}
