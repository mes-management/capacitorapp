import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OccupantPendingcomplaintDataPage } from './occupant-pendingcomplaint-data.page';

describe('OccupantPendingcomplaintDataPage', () => {
  let component: OccupantPendingcomplaintDataPage;
  let fixture: ComponentFixture<OccupantPendingcomplaintDataPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupantPendingcomplaintDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OccupantPendingcomplaintDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
