import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OccupantPendingcomplaintDataPageRoutingModule } from './occupant-pendingcomplaint-data-routing.module';

import { OccupantPendingcomplaintDataPage } from './occupant-pendingcomplaint-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OccupantPendingcomplaintDataPageRoutingModule
  ],
  declarations: [OccupantPendingcomplaintDataPage]
})
export class OccupantPendingcomplaintDataPageModule {}
