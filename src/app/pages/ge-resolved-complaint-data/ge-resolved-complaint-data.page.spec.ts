import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GeResolvedComplaintDataPage } from './ge-resolved-complaint-data.page';

describe('GeResolvedComplaintDataPage', () => {
  let component: GeResolvedComplaintDataPage;
  let fixture: ComponentFixture<GeResolvedComplaintDataPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GeResolvedComplaintDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GeResolvedComplaintDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
