import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeResolvedComplaintDataPage } from './ge-resolved-complaint-data.page';

const routes: Routes = [
  {
    path: '',
    component: GeResolvedComplaintDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeResolvedComplaintDataPageRoutingModule {}
