import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GeResolvedComplaintDataPageRoutingModule } from './ge-resolved-complaint-data-routing.module';

import { GeResolvedComplaintDataPage } from './ge-resolved-complaint-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeResolvedComplaintDataPageRoutingModule
  ],
  declarations: [GeResolvedComplaintDataPage]
})
export class GeResolvedComplaintDataPageModule {}
