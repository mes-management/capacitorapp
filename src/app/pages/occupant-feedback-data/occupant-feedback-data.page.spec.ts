import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OccupantFeedbackDataPage } from './occupant-feedback-data.page';

describe('OccupantFeedbackDataPage', () => {
  let component: OccupantFeedbackDataPage;
  let fixture: ComponentFixture<OccupantFeedbackDataPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupantFeedbackDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OccupantFeedbackDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
