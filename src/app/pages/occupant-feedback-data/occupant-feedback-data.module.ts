import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OccupantFeedbackDataPageRoutingModule } from './occupant-feedback-data-routing.module';

import { OccupantFeedbackDataPage } from './occupant-feedback-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OccupantFeedbackDataPageRoutingModule
  ],
  declarations: [OccupantFeedbackDataPage]
})
export class OccupantFeedbackDataPageModule {}
