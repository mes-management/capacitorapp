import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GEViewPendingComplaintsPage } from './geview-pending-complaints.page';

const routes: Routes = [
  {
    path: '',
    component: GEViewPendingComplaintsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GEViewPendingComplaintsPageRoutingModule {}
