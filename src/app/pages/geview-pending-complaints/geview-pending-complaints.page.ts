import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-geview-pending-complaints',
  templateUrl: './geview-pending-complaints.page.html',
  styleUrls: ['./geview-pending-complaints.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService],
})
export class GEViewPendingComplaintsPage implements OnInit {
  errorMessage:any;
  Remarks:any;
  Comment:any;
  regData = {Comments:'',Status:'',USR_ID:'',ComplaintCode:''}
  Occupantdata={
    OccupantName: "",
    EmailID: "",
    MobileNumber: "",
     Location: "",
    ComplaintDescription: "",
    CreatedDate: "",
    ServiceTypeName: "",
    AccomodationType: "",
    AccomodationNo: "",
    Status:"",
    
        };
  constructor(public OccupantService:OccupantRegistrationService,private route :Router,public loadingController: LoadingController) { }

  ngOnInit() {
    let ComplaintCode=localStorage.getItem('complaintCode');
   
    this.OccupantService.GetGEPendingComplaintsData(ComplaintCode).subscribe(res =>
      {
        if(res){
       
          this.Occupantdata.OccupantName=res[0].name;
         // this.Occupantdata.EmailID=res[0].emailID;
          this.Occupantdata.MobileNumber=res[0].mobileNumber;
          this.Occupantdata.Location=res[0].locationName;
          this.Occupantdata.ComplaintDescription=res[0].complaintDescription;
          this.Occupantdata.CreatedDate=res[0].createdDate;
          this.Occupantdata.ServiceTypeName=res[0].serviceTypeName;
          this.Occupantdata.AccomodationType=res[0].accommodationType;
          this.Occupantdata.AccomodationNo=res[0].accommodationNumber;
         
        }
        
      });
  }
  goback(){
    this.route.navigateByUrl('view-complaints');
  }
  Assign(){
this.regData.Comments=this.Comment;
   let USR_ID= localStorage.getItem('usR_ID');
   this.regData.USR_ID=USR_ID;
   this.regData.ComplaintCode=localStorage.getItem('complaintCode');
   
    
    this.OccupantService.AddGeapprovedComplaints(this.regData).subscribe((result) => {
 
      alert("Your Request for ComplaintCode '" +this.regData.ComplaintCode+ "' Status has been Updated Successfully");
      
      if(this.regData.Status=="Resolved"){
        

        this.route.navigate(['view-geresolved-complaints'])
        .then(() => {
          window.location.reload();
         
        });
      //  this.route.navigateByUrl('');
  
      }
      else if(this.regData.Status=="Work In Progress"){
        
        this.route.navigate(['view-geworkinprogress-complaints'])
        .then(() => {
          window.location.reload();
         
        });

        //this.route.navigateByUrl('view-geworkinprogress-complaints');
      }
      
     // this.route.navigateByUrl('view-complaints');
  
    
   }, (err) => {
   
    
   });
 }
}
