import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GEViewPendingComplaintsPage } from './geview-pending-complaints.page';

describe('GEViewPendingComplaintsPage', () => {
  let component: GEViewPendingComplaintsPage;
  let fixture: ComponentFixture<GEViewPendingComplaintsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GEViewPendingComplaintsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GEViewPendingComplaintsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
