import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GEViewPendingComplaintsPageRoutingModule } from './geview-pending-complaints-routing.module';

import { GEViewPendingComplaintsPage } from './geview-pending-complaints.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GEViewPendingComplaintsPageRoutingModule
  ],
  declarations: [GEViewPendingComplaintsPage]
})
export class GEViewPendingComplaintsPageModule {}
