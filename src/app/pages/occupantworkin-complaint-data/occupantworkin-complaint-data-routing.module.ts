import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OccupantworkinComplaintDataPage } from './occupantworkin-complaint-data.page';

const routes: Routes = [
  {
    path: '',
    component: OccupantworkinComplaintDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OccupantworkinComplaintDataPageRoutingModule {}
