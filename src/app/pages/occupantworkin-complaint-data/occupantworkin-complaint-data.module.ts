import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OccupantworkinComplaintDataPageRoutingModule } from './occupantworkin-complaint-data-routing.module';

import { OccupantworkinComplaintDataPage } from './occupantworkin-complaint-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OccupantworkinComplaintDataPageRoutingModule
  ],
  declarations: [OccupantworkinComplaintDataPage]
})
export class OccupantworkinComplaintDataPageModule {}
