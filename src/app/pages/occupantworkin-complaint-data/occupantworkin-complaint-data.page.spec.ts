import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OccupantworkinComplaintDataPage } from './occupantworkin-complaint-data.page';

describe('OccupantworkinComplaintDataPage', () => {
  let component: OccupantworkinComplaintDataPage;
  let fixture: ComponentFixture<OccupantworkinComplaintDataPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupantworkinComplaintDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OccupantworkinComplaintDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
