import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-geworking-progress-complaints',
  templateUrl: './geworking-progress-complaints.page.html',
  styleUrls: ['./geworking-progress-complaints.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService],
})
export class GEWorkingProgressComplaintsPage implements OnInit {
  errorMessage:any;
  Remarks:any;
  Comment:any;
  photo:any;
  regData={ComplaintCode:'',Comments:'',USR_ID:0, FileDocument:''};
  
  //regData = {Comments:'',USR_ID:'',ComplaintCode:''}
  Occupantdata={
    OccupantName: "",
    EmailID: "",
    MobileNumber: "",
     Location: "",
    ComplaintDescription: "",
    CreatedDate: "",
    ServiceTypeName: "",
    AccomodationType: "",
    AccomodationNo: "",
    Status:"",
    
        };
  constructor(public OccupantService:OccupantRegistrationService,private route :Router,public loadingController: LoadingController) { }

  ngOnInit() {
    let ComplaintCode=localStorage.getItem('complaintCode');
   
    this.OccupantService.GetGEWorkinprogressComplaintsdata(ComplaintCode).subscribe(res =>
      {
        if(res){
       
          this.Occupantdata.OccupantName=res[0].name;
         // this.Occupantdata.EmailID=res[0].emailID;
          this.Occupantdata.MobileNumber=res[0].mobileNumber;
          this.Occupantdata.Location=res[0].locationName;
          this.Occupantdata.ComplaintDescription=res[0].complaintDescription;
          this.Occupantdata.CreatedDate=res[0].createdDate;
          this.Occupantdata.ServiceTypeName=res[0].serviceTypeName;
          this.Occupantdata.AccomodationType=res[0].accommodationType;
          this.Occupantdata.AccomodationNo=res[0].accommodationNumber;
         
        }
        
      });
  }

  onFileChoose(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    const pattern = /image-*/;
    const reader = new FileReader();

    if (!file.type.match(pattern)) {
      console.log('File format not supported');
      return;
    }
    reader.onload = () => {
      this.photo = reader.result.toString();
 
      this.regData.FileDocument=this.photo;
    //alert(this.photo)
   
    };
    reader.readAsDataURL(file);
    
  }


  goback(){
    this.route.navigateByUrl('view-geworkinprogress-complaints');
  }
  Assign(){
    this.regData.Comments=this.Comment;
   let USR_ID= localStorage.getItem('usR_ID');
   this.regData.USR_ID=parseInt(USR_ID);
   this.regData.ComplaintCode=localStorage.getItem('complaintCode');
   this.regData.FileDocument=this.photo
   // alert(this.regData.FileDocument)

    this.OccupantService.AddGEWorkinprogress(this.regData).then((result) => {
 
      alert("Your Request for ComplaintCode '"+this.regData.ComplaintCode+"' is Resolved");
      this.route.navigateByUrl('view-geresolved-complaints');
    
   }, (err) => {
   
    
   });
 }
}
