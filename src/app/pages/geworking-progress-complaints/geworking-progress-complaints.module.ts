import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GEWorkingProgressComplaintsPageRoutingModule } from './geworking-progress-complaints-routing.module';

import { GEWorkingProgressComplaintsPage } from './geworking-progress-complaints.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GEWorkingProgressComplaintsPageRoutingModule
  ],
  declarations: [GEWorkingProgressComplaintsPage]
})
export class GEWorkingProgressComplaintsPageModule {}
