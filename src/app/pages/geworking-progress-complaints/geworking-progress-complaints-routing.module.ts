import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GEWorkingProgressComplaintsPage } from './geworking-progress-complaints.page';

const routes: Routes = [
  {
    path: '',
    component: GEWorkingProgressComplaintsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GEWorkingProgressComplaintsPageRoutingModule {}
