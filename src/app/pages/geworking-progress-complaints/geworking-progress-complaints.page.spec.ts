import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GEWorkingProgressComplaintsPage } from './geworking-progress-complaints.page';

describe('GEWorkingProgressComplaintsPage', () => {
  let component: GEWorkingProgressComplaintsPage;
  let fixture: ComponentFixture<GEWorkingProgressComplaintsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GEWorkingProgressComplaintsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GEWorkingProgressComplaintsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
