import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OccupantregistrationPage } from './occupantregistration.page';

const routes: Routes = [
  {
    path: '',
    component: OccupantregistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OccupantregistrationPageRoutingModule {}
