import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OccupantregistrationPageRoutingModule } from './occupantregistration-routing.module';

import { OccupantregistrationPage } from './occupantregistration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OccupantregistrationPageRoutingModule
  ],
  declarations: [OccupantregistrationPage]
})
export class OccupantregistrationPageModule {}
