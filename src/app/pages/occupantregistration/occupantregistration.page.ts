import { Component, Input, NgModule, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Navigation } from 'selenium-webdriver';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-occupantregistration',
  templateUrl: './occupantregistration.page.html',
  styleUrls: ['./occupantregistration.page.scss'],
  providers:[OccupantRegistrationService]
})
export class OccupantregistrationPage implements OnInit {
 
  isSubmitted  = false;
  
  loading: any;
  locationData:any;
  complexData:any;
  AccomodationTypes:any;
  AccomodationData:any;
  divisionData:any;
  subdivisionData:any;
  errorMessage: string; 
  locationType:any;
  div:any;
  regData = {officeID:'',LocationID:'',complexID:'',accommodationType:'',AccommodationID:''}
  constructor(private fb: FormBuilder,private route :Router,public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,public OccupantService:OccupantRegistrationService) {
    
  }
  ngOnInit() {  
    
    // call the method on initial load of page to bind dropdown  
    this.clear();
    this.getDivision();
    //this.getAccomdationTypes();
   
    
    }
    
    getDivision(){
      this.OccupantService.getDivisiondetails().subscribe(res =>
        this.divisionData = res, 
        error => this.errorMessage = <any>error); 
    }
    
  
  
 
  onSelectChangeDivision(data :any){
    //alert(JSON.stringify(data.flatID))
    this.regData.officeID=data.officeID;
    this.OccupantService.getlocationdetails(data.officeID).subscribe(res =>
      this.locationData = res, 
      error => this.errorMessage = <any>error); 
  }
  onSelectChangeLocation(data :any){
   
    this.regData.LocationID=data.locationID;
    this.OccupantService.getcomplexdetails(data.locationID).subscribe(res =>
      this.complexData = res, 
      error => this.errorMessage = <any>error); 
     
    
  }
  onSelectChangeComplex(data :any){
   
    this.regData.complexID=data.complexID;

  

    
    this.OccupantService.getAccomdationTypes(data.complexID).subscribe(res =>
      this.AccomodationTypes = res, 
 
      error => this.errorMessage = <any>error); 
    
      if(this.regData.accommodationType=="")
      {
        this.OccupantService.AccomdationData11(this.regData.complexID).subscribe(res =>
          this.AccomodationData = res, 
          error => this.errorMessage = <any>error); 
      }
  
  }
  onSelectChangeBuilding(data:any){
   




    this.regData.accommodationType=data.accommodationType;
   
    this.OccupantService.AccomdationData(this.regData.complexID,data.accommodationType).subscribe(res =>
      this.AccomodationData = res, 
      error => this.errorMessage = <any>error); 
    

  } 
  onSelectChangeAccom(data :any){
     this.regData.AccommodationID=data.accommodationID;
    
    
  }
 
  getSubDivision(locationId:any,divisionID:any){
    this.OccupantService.getSubDivisiondetails(divisionID).subscribe(res =>
      this.subdivisionData = res, 
      error => this.errorMessage = <any>error); 
  }
ionViewDidLoad() {
  console.log('ionViewDidLoad OccupantRegistrationPage');
}
  AddOccupant(){
    this.isSubmitted  = true;
   
    if(this.regData.officeID=='' || this.regData.LocationID=='' || this.regData.complexID==''){
     alert('Mandatory fields required');
    }
    else{
      localStorage.setItem('officeID',this.regData.officeID);
      localStorage.setItem('LocationID',this.regData.LocationID);
      localStorage.setItem('complexID',this.regData.complexID);
      localStorage.setItem('accommodationType',this.regData.accommodationType);
      localStorage.setItem('AccommodationID',this.regData.AccommodationID);
       this.route.navigateByUrl('occupantregdetails');
    }
    
   
   
  }
 
clear(){
 
  localStorage.setItem('officeID','');
  localStorage.setItem('LocationID','');
  localStorage.setItem('complexID','');
  localStorage.setItem('accommodationType','');
  localStorage.setItem('Acc_DetailsID','');
 

}
 
}
