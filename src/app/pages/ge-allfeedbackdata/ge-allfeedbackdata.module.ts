import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GeAllfeedbackdataPageRoutingModule } from './ge-allfeedbackdata-routing.module';

import { GeAllfeedbackdataPage } from './ge-allfeedbackdata.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeAllfeedbackdataPageRoutingModule
  ],
  declarations: [GeAllfeedbackdataPage]
})
export class GeAllfeedbackdataPageModule {}
