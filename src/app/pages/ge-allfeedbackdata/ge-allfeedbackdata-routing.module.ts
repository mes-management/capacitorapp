import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeAllfeedbackdataPage } from './ge-allfeedbackdata.page';

const routes: Routes = [
  {
    path: '',
    component: GeAllfeedbackdataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeAllfeedbackdataPageRoutingModule {}
