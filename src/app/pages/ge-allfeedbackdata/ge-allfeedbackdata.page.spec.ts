import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GeAllfeedbackdataPage } from './ge-allfeedbackdata.page';

describe('GeAllfeedbackdataPage', () => {
  let component: GeAllfeedbackdataPage;
  let fixture: ComponentFixture<GeAllfeedbackdataPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GeAllfeedbackdataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GeAllfeedbackdataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
