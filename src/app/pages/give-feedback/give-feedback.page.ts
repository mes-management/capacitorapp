import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ViewEncapsulation } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';
import { AuthServiceService } from 'src/app/providers/auth-service.service';

@Component({
  selector: 'app-give-feedback',
  templateUrl: './give-feedback.page.html',
  styleUrls: ['./give-feedback.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService],
  encapsulation: ViewEncapsulation.None
})
export class GiveFeedbackPage {


  public columns: any;
  public rows: any;
  errorMessage: string; 
  complaintfeedback:any;
  regData = {ServiceTypeName:'',Complaint_Description:'',CreatedDate:'',ComplaintID:''}
  
  constructor(
   private http: HttpClient,public OccupantService:OccupantRegistrationService,private route :Router
  ) {
    this.columns = [
      {name:'ComplaintCode'},
      { name: 'ServiceTypeName' },
      { name: 'Status' },
<<<<<<< HEAD
      //  { name: 'complaintDescription' }
=======
       { name: 'complaintDescription' }
>>>>>>> 433948a77c4b4afad20f042cd316eb052ae61df0

    ];

     

  }
   

  ngOnInit() {
    let OccupantID=localStorage.getItem('occupantID')
    this.OccupantService.getComplaintFeedback(OccupantID).subscribe(res =>
      this.rows = res,
     
      error => this.errorMessage = <any>error);
     // alert(this.rows.complaintDescription)
  }
  
  onActivate(event) {
    if(event.type == 'click') {
      let ComplaintCode=event.row.complaintCode;
       localStorage.setItem('complaintCode',ComplaintCode);
       let ServiceTypeName=event.row.serviceTypeName;
       console.log(event.row)
       localStorage.setItem('ServiceTypeName',ServiceTypeName);
       let Complaint_Description=event.row.complaintDescription;
       localStorage.setItem('Complaint_Description',Complaint_Description);
       
      this.route.navigateByUrl('feedback');
    
   }
   
}





}
