import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GiveFeedbackPageRoutingModule } from './give-feedback-routing.module';

import { GiveFeedbackPage } from './give-feedback.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GiveFeedbackPageRoutingModule,
    NgxDatatableModule,
  ],
  declarations: [GiveFeedbackPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GiveFeedbackPageModule {}
