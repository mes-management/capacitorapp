import { AfterViewInit, Component, OnInit,ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ViewEncapsulation } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
//import { LoadingService } from '../providers/LoadingService';
@Component({
  selector: 'app-view-complaints',
  templateUrl: './view-complaints.page.html',
  styleUrls: ['./view-complaints.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService],
  encapsulation: ViewEncapsulation.None
})
export class ViewComplaintsPage  {
  @ViewChild('search', { static: false }) search: any;
  public columns: any;
  public rows: any;
  errorMessage: string; 
  complaintfeedback:any;
  regData = {ServiceTypeName:'',Complaint_Description:'',CreatedDate:''}
  complaintID:any;
  name = 'Ngx Datatables Filter All Columns';
  public temp: Array<object> = [];
  // public rows: Array<object> = [];
  // public columns: Array<object>;

  constructor(
   private http: HttpClient,public OccupantService:OccupantRegistrationService,private route :Router
  ) {
    this.columns = [
      { name: 'complaintCode' },
      { name: 'ServiceTypeName' },
     // {name:'accommodationNumber'},
    //   {name:'accommodationType'},
    //  { name: 'locationName'},
    //  { name: 'divisionName' },
      {name:'complexName'},
    //  {name:'status'},
      
    ];

     

  }
   

  ngOnInit() {
   // this.loading.present();
   let OccupantID= localStorage.getItem('occupantID');
let USR_ID=localStorage.getItem('usR_ID')
    this.OccupantService.GetAllPendingComplaints(USR_ID).subscribe(res =>
      this.rows = res, 
   error => this.errorMessage = <any>error);

  }
  
  // ngAfterViewInit(): void {
  //   // Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
  //   // Add 'implements AfterViewInit' to the class.
  //   fromEvent(this.search.nativeElement, 'keydown')
  //     .pipe(
  //       debounceTime(550),
  //       map(x => x['target']['value'])
  //     )
  //     .subscribe(value => {
  //       this.updateFilter(value);
  //     });
  // }

  // updateFilter(val: any) {
  //   const value = val.toString().toLowerCase().trim();
  //   // get the amount of columns in the table
  //   const count = this.columns.length;
  //   // get the key names of each column in the dataset
  //   const keys = Object.keys(this.temp[0]);
  //   // assign filtered matches to the active datatable
  //   this.rows = this.temp.filter(item => {
  //     // iterate through each row's column data
  //     for (let i = 0; i < count; i++) {
  //       // check for a match
  //       if (
  //         (item[keys[i]] &&
  //           item[keys[i]]
  //             .toString()
  //             .toLowerCase()
  //             .indexOf(value) !== -1) ||
  //         !value
  //       ) {
  //         // found match, return true to add to result set
  //         return true;
  //       }
  //     }
  //   });

  //   // Whenever the filter changes, always go back to the first page
  //   // this.table.offset = 0;
  // }









  onActivate(event) {
    if(event.type == 'click') {
      let complaintCode=event.row.complaintCode;
      localStorage.setItem('complaintCode',complaintCode);
      this.route.navigateByUrl('geview-pending-complaints')
    }
   
}





}
