import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewComplaintsPageRoutingModule } from './view-complaints-routing.module';

import { ViewComplaintsPage } from './view-complaints.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewComplaintsPageRoutingModule,
    NgxDatatableModule,
  ],
  declarations: [ViewComplaintsPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})


export class ViewComplaintsPageModule {}
