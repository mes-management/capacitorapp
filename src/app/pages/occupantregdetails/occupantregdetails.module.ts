import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OccupantregdetailsPageRoutingModule } from './occupantregdetails-routing.module';

import { OccupantregdetailsPage } from './occupantregdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    OccupantregdetailsPageRoutingModule
  ],
  declarations: [OccupantregdetailsPage]
})
export class OccupantregdetailsPageModule {}
