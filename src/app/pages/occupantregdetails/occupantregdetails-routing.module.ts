import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OccupantregdetailsPage } from './occupantregdetails.page';

const routes: Routes = [
  {
    path: '',
    component: OccupantregdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OccupantregdetailsPageRoutingModule {}
