import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { LoadingService } from 'src/app/providers/LoadingService';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-occupantregdetails',
  templateUrl: './occupantregdetails.page.html',
  styleUrls: ['./occupantregdetails.page.scss'],
  providers:[OccupantRegistrationService]
})
export class OccupantregdetailsPage implements OnInit {
  
  ionicForm: FormGroup;
  isSubmitted = false;
  mobileAlredyExist:any;
  errorMessage:any;
  regData = {OfficeID:'',LocationID:'',ComplexID:'',AccommodationType:'',AccommodationID:'',Name:'',EmailID:'',MobileNumber:'',Password:''}
  constructor(private formBuilder: FormBuilder,private route :Router,public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,public OccupantService:OccupantRegistrationService,public loading: LoadingService) {
    
    }
  ngOnInit() {  
    // call the method on initial load of page to bind dropdown 
    this.ionicForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      mobile: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    })  
   this.regData.OfficeID=localStorage.getItem('officeID');
   this.regData.LocationID=localStorage.getItem('LocationID');
   this.regData.ComplexID=localStorage.getItem('complexID');
   this.regData.AccommodationType=localStorage.getItem('accommodationType');
   this.regData.AccommodationID=localStorage.getItem('AccommodationID');
    }
   
    get errorControl() {
      return this.ionicForm.controls;
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OccupantRegistrationPage');
  }
  onSelectMobileChange(e){
   if(e!=''){
    this.OccupantService.checkMobileExists(e).subscribe(res =>
      {
        if(res=="1"){
         this.mobileAlredyExist="Mobile Number Already Exist";
        }
        if(res=="0"){
         this.mobileAlredyExist="";
        }
      });
   }
   
      
   
  }
  AddOccupant(){
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      if(this.mobileAlredyExist==''){
        this.loading.present();
        this.regData.Name=this.ionicForm.value.name;
        this.regData.EmailID=this.ionicForm.value.email;
        this.regData.MobileNumber=this.ionicForm.value.mobile;
        this.regData.Password=this.ionicForm.value.password;
        this.OccupantService.AddOccupant(this.regData).then((result) => {
          this.loading.dismiss();
        alert("Your Request for Registration has been  Submitted Successfully");
        this.clear();
        this.route.navigate([''])
        .then(() => {
          window.location.reload();
         
        });
      
      // this.
     }, (err) => {
     
      this.loading.dismiss();
     });
      }
     
     
     
   }
   
   }
  
   clear(){
    // localStorage.setItem('officeID','');
    // localStorage.setItem('LocationID','');
    // localStorage.setItem('complexID','');
    // localStorage.setItem('accommodationType','');
    // localStorage.setItem('Acc_DetailsID','');
    // this.regData.Name='';
    // this.regData.EmailID='';
    // this.regData.MobileNumber='';
    // this.regData.Password='';

  }
}
