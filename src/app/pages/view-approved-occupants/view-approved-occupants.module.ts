import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewApprovedOccupantsPageRoutingModule } from './view-approved-occupants-routing.module';

import { ViewApprovedOccupantsPage } from './view-approved-occupants.page';
import { HttpClientModule } from '@angular/common/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxDatatableModule,
    ViewApprovedOccupantsPageRoutingModule
  ],
  declarations: [ViewApprovedOccupantsPage]
})
export class ViewApprovedOccupantsPageModule {}
