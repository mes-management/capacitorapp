import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewApprovedOccupantsPage } from './view-approved-occupants.page';

describe('ViewApprovedOccupantsPage', () => {
  let component: ViewApprovedOccupantsPage;
  let fixture: ComponentFixture<ViewApprovedOccupantsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewApprovedOccupantsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewApprovedOccupantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
