import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Data, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { LoadingService } from 'src/app/providers/LoadingService';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-view-approved-occupants',
  templateUrl: './view-approved-occupants.page.html',
  styleUrls: ['./view-approved-occupants.page.scss'],
  providers:[OccupantRegistrationService],
  encapsulation: ViewEncapsulation.None
})
export class ViewApprovedOccupantsPage implements OnInit {

  
  public columns: any;
  public rows: any;
  errorMessage:any;
  constructor(private http: HttpClient,public OccupantService:OccupantRegistrationService,private route :Router,public loading: LoadingService) {
    this.columns = [
    
      { name: 'name' },
      { name: 'mobileNumber' },
      //{ name: 'remarks' },
    //  { name: 'createdDate' },
      { name: 'status' },
     
    ];
   
     
  }
  ngOnInit() {
    let USR_ID=localStorage.getItem('usR_ID');
    this.loading.present();
    this.OccupantService.ApprovedOccupantDetails(USR_ID).subscribe(res =>
      {
       
        this.rows=res;
        this.loading.dismiss();
    }, (err) => {
      this.loading.dismiss();
    });
  }
  
  onActivate(event) {
    if(event.type == 'click') {
       let MobileNumber=event.row.mobileNumber;
       localStorage.setItem('mobileNumber',MobileNumber);
<<<<<<< HEAD
      // alert(MobileNumber)
=======
       alert(MobileNumber)
>>>>>>> 433948a77c4b4afad20f042cd316eb052ae61df0
       this.route.navigateByUrl('view-approved-data');
    }
}



}
