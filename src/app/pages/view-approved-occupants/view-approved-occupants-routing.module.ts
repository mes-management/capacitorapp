import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewApprovedOccupantsPage } from './view-approved-occupants.page';

const routes: Routes = [
  {
    path: '',
    component: ViewApprovedOccupantsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewApprovedOccupantsPageRoutingModule {}
