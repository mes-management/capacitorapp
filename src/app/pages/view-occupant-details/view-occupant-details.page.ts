import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { saveConfig } from '@ionic/core';
import { LoadingService } from 'src/app/providers/LoadingService';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-view-occupant-details',
  templateUrl: './view-occupant-details.page.html',
  styleUrls: ['./view-occupant-details.page.scss'],
})
export class ViewOccupantDetailsPage implements OnInit {
  errorMessage:any;
  Remarks:any;
  regData = {OccupantID:'',Remarks:'',Status:''}
  Occupantdata={
    OccupantName: "",
    EmailID: "",
    MobileNumber: "",
    Location: "",
    Division: "",
    CreatedDate: "",
    ComplexName: "",
    AccomodationType: "",
    AccomodationNo: "",
    
        };
  constructor(public OccupantService:OccupantRegistrationService,private route :Router,public loadingController: LoadingController,public loading: LoadingService) { }

  ngOnInit() {
    let OccupantID=localStorage.getItem('OccupantID');
    this.OccupantService.GetOccupantDetailsByOccupantID(OccupantID).subscribe(res =>
      {
        if(res){
       
          this.Occupantdata.OccupantName=res[0].name;
          this.Occupantdata.EmailID=res[0].emailID;
          this.Occupantdata.MobileNumber=res[0].mobileNumber;
          this.Occupantdata.Location=res[0].location;
          this.Occupantdata.Division=res[0].officeType;
          this.Occupantdata.CreatedDate=res[0].createdDate;
          this.Occupantdata.ComplexName=res[0].complexName;
          this.Occupantdata.AccomodationType=res[0].accommodationType;
          this.Occupantdata.AccomodationNo=res[0].accommodationNumber;
         
        }
        
      });
  }
 
 

 

}
