import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewOccupantDetailsPage } from './view-occupant-details.page';

const routes: Routes = [
  {
    path: '',
    component: ViewOccupantDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewOccupantDetailsPageRoutingModule {}
