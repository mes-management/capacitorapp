import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewOccupantDetailsPageRoutingModule } from './view-occupant-details-routing.module';

import { ViewOccupantDetailsPage } from './view-occupant-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewOccupantDetailsPageRoutingModule
  ],
  declarations: [ViewOccupantDetailsPage]
})
export class ViewOccupantDetailsPageModule {}
