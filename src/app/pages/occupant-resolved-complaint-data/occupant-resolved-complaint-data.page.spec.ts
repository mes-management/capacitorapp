import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OccupantResolvedComplaintDataPage } from './occupant-resolved-complaint-data.page';

describe('OccupantResolvedComplaintDataPage', () => {
  let component: OccupantResolvedComplaintDataPage;
  let fixture: ComponentFixture<OccupantResolvedComplaintDataPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupantResolvedComplaintDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OccupantResolvedComplaintDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
