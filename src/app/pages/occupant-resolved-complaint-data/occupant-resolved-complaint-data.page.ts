import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-occupant-resolved-complaint-data',
  templateUrl: './occupant-resolved-complaint-data.page.html',
  styleUrls: ['./occupant-resolved-complaint-data.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService],
})
export class OccupantResolvedComplaintDataPage implements OnInit {
  errorMessage:any;
  Remarks:any;
  Comment:any;
  regData = {Comments:'',Status:'',USR_ID:'',ComplaintCode:''}
  Occupantdata={
    OccupantName: "",
    EmailID: "",
    MobileNumber: "",
     Location: "",
    ComplaintDescription: "",
    CreatedDate: "",
    ServiceTypeName: "",
    AccomodationType: "",
    AccomodationNo: "",
    Status:"",
    
        };
  constructor(public OccupantService:OccupantRegistrationService,private route :Router,public loadingController: LoadingController) { }

 
  ngOnInit() {
    let ComplaintCode=localStorage.getItem('complaintCode');
  // alert(ComplaintCode)
    this.OccupantService.GetOccupantResolvedComplaintsData(ComplaintCode).subscribe(res =>
      {
        if(res){
       
          this.Occupantdata.OccupantName=res[0].name;
         // this.Occupantdata.EmailID=res[0].emailID;
          this.Occupantdata.MobileNumber=res[0].mobileNumber;
          this.Occupantdata.Location=res[0].locationName;
          this.Occupantdata.ComplaintDescription=res[0].complaintDescription;
          this.Occupantdata.CreatedDate=res[0].createdDate;
          this.Occupantdata.ServiceTypeName=res[0].serviceTypeName;
          this.Occupantdata.AccomodationType=res[0].accommodationType;
          this.Occupantdata.AccomodationNo=res[0].accommodationNumber;
         this.Occupantdata.Status=res[0].status;
         this.Occupantdata.ComplaintDescription=res[0].complaintDescription;
        }
        
      });
  }

}
