import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OccupantResolvedComplaintDataPage } from './occupant-resolved-complaint-data.page';

const routes: Routes = [
  {
    path: '',
    component: OccupantResolvedComplaintDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OccupantResolvedComplaintDataPageRoutingModule {}
