import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OccupantResolvedComplaintDataPageRoutingModule } from './occupant-resolved-complaint-data-routing.module';

import { OccupantResolvedComplaintDataPage } from './occupant-resolved-complaint-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OccupantResolvedComplaintDataPageRoutingModule
  ],
  declarations: [OccupantResolvedComplaintDataPage]
})
export class OccupantResolvedComplaintDataPageModule {}
