import { Component, Input, EventEmitter ,Output, OnInit, COMPILER_OPTIONS} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { LoadingController, ToastController } from "@ionic/angular";
import { AuthServiceService } from "src/app/providers/auth-service.service";
import { OccupantRegistrationService } from "src/app/providers/occupant-registration.service";
enum COLORS{
  GREY="#E0E0E0",
  GREEN="#76FF03",
  YELLOW="#FFCA28",
  RED="#DD2C00"
}
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService]
 
})
export class FeedbackPage implements OnInit {
  ionicForm: FormGroup;
  isSubmitted = false;
  @Input() rating: number ;
  ServiceTypeName:any;
  complaintDescription:any;
  complaintID:any;

  @Output() ratingChange: EventEmitter<number> = new EventEmitter();
regData={UserID:'',ComplaintCode:'',RatingNumber:'',Remarks:'',Rating:''};
OccupantRemarks: any;
 
  constructor(private formBuilder: FormBuilder,private route :Router,public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,public OccupantService:OccupantRegistrationService) {
      this.ServiceTypeName=localStorage.getItem('ServiceTypeName');
      this.complaintDescription=localStorage.getItem('Complaint_Description');
      //this.complaintCode=localStorage.getItem('complaintCode');
    }

  rate(index: number) {
    this.rating=index;
   
    this.ratingChange.emit(this.rating);
     
   }
     
  getColor(index: number) {
     
    if(this.isAboveRating(index)){
      return COLORS.GREY;
    }
    switch(this.rating){
      case 1:
        case 2:
          return COLORS.RED;
          case 3:
          return COLORS.YELLOW;
          case 4:
          return COLORS.GREEN;
          case 5:
          return COLORS.GREEN;
          default:
            return COLORS.GREY;
    }
    }

  isAboveRating(index: number): boolean {
    return index>this.rating;
   }
  
  ngOnInit() {
   this.regData.ComplaintCode =localStorage.getItem('complaintCode');
   this.ServiceTypeName=localStorage.getItem('ServiceTypeName');
   //alert(this.ServiceTypeName);
    this.ionicForm = this.formBuilder.group({
      ComplaintDescription: [''],
      ServiceType:[''],
      Comments: [''] 
    }) 
   
  }
  AddFeedback(){

    this.regData.Remarks=this.OccupantRemarks;
this.regData.UserID=localStorage.getItem('usR_ID');
this.regData.ComplaintCode=localStorage.getItem('complaintCode');

    this.regData.RatingNumber=this.rating.toString();

    this.OccupantService.AddFeedbackInfo(this.regData).then((result) => {
 
      alert("Feedback for this Complaint Submitted Successfully");
      
    this.route.navigateByUrl('view-feedback-details');
    
   }, (err) => {
   
    
   });
   this.clear();
 }

 
 clear(){
 
  localStorage.setItem('complaint','');
  localStorage.setItem('ServiceTypeName','');
  localStorage.setItem('Complaint_Description','');
  

}

 }