import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { saveConfig } from '@ionic/core';
import { LoadingService } from 'src/app/providers/LoadingService';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-admin-approve-occupant',
  templateUrl: './admin-approve-occupant.page.html',
  styleUrls: ['./admin-approve-occupant.page.scss'],
  providers:[OccupantRegistrationService],
})
export class AdminApproveOccupantPage implements OnInit {
  errorMessage:any;
  Remarks:any;
  regData = {OccupantID:'',Remarks:'',Status:''}
  Occupantdata={
    OccupantName: "",
    EmailID: "",
    MobileNumber: "",
    Location: "",
    Division: "",
    CreatedDate: "",
    ComplexName: "",
    BuildingName: "",
    AccomodationNo: "",
    
        };
  constructor(public OccupantService:OccupantRegistrationService,private route :Router,public loadingController: LoadingController,public loading: LoadingService) { }

  ngOnInit() {
    let mobileNumber=localStorage.getItem('mobileNumber');
    this.OccupantService.GetOccupantDetailsByMobileNumber(mobileNumber).subscribe(res =>
      {
        if(res){
      // alert(JSON.stringify(res))
          this.Occupantdata.OccupantName=res[0].name;
          this.Occupantdata.EmailID=res[0].emailID;
          this.Occupantdata.MobileNumber=res[0].mobileNumber;
          this.Occupantdata.Location=res[0].location;
          this.Occupantdata.Division=res[0].officeType;
          this.Occupantdata.CreatedDate=res[0].createdDate;
          this.Occupantdata.ComplexName=res[0].complexName;
          this.Occupantdata.BuildingName=res[0].buildingName;
          this.Occupantdata.AccomodationNo=res[0].accommodationNumber;
          this.regData.OccupantID=res[0].occupant;
         
        }
        
      });
  }
  Approve(){
 
    this.regData.Status="Approved";
   this.save();
  }
  Reject(){
    this.regData.Status="Rejected";
    this.save();
  }
  save(){
    this.loading.present();
    // this.regData.OccupantID=localStorage.getItem('OccupantID');
    this.regData.Remarks=this.Remarks;
  
  this.OccupantService.UpdateOccupantApprovalStatus(this.regData).subscribe(res =>
    {
     
      this.loading.dismiss();
      if(this.regData.Status=="Approved"){
        this.route.navigateByUrl('view-approved-occupants');
      }
      if(this.regData.Status=="Rejected"){
        this.route.navigateByUrl('view-rejected-occupants');
      }
    
    }, (err) => {
      this.loading.dismiss();
    });  
  }

 
}
