import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminApproveOccupantPage } from './admin-approve-occupant.page';

const routes: Routes = [
  {
    path: '',
    component: AdminApproveOccupantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminApproveOccupantPageRoutingModule {}
