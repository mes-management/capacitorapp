import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminApproveOccupantPageRoutingModule } from './admin-approve-occupant-routing.module';

import { AdminApproveOccupantPage } from './admin-approve-occupant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminApproveOccupantPageRoutingModule
  ],
  declarations: [AdminApproveOccupantPage]
})
export class AdminApproveOccupantPageModule {}
