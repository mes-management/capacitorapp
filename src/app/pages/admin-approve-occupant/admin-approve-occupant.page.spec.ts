import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdminApproveOccupantPage } from './admin-approve-occupant.page';

describe('AdminApproveOccupantPage', () => {
  let component: AdminApproveOccupantPage;
  let fixture: ComponentFixture<AdminApproveOccupantPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminApproveOccupantPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdminApproveOccupantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
