import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService],
})
export class MyProfilePage implements OnInit {
  Occupantdata={
    OccupantName: "",
    EmailID: "",
    MobileNumber: "",
     Location: "",
    ComplaintDescription: "",
    CreatedDate: "",
    ServiceTypeName: "",
    AccomodationType: "",
    AccomodationNo: "",
    Status:"",
    ComplexName:"",
    BuildingName:"",
    
        };
  constructor(public OccupantService:OccupantRegistrationService,private route :Router,public loadingController: LoadingController) { }

  ngOnInit() {
    let mobileNumber=localStorage.getItem('mobileNumber');
    this.OccupantService.GetOccupantDetailsByMobileNumber(mobileNumber).subscribe(res =>
      {
        if(res){
       
          this.Occupantdata.OccupantName=res[0].name;
         // this.Occupantdata.EmailID=res[0].emailID;
          this.Occupantdata.MobileNumber=res[0].mobileNumber;
          this.Occupantdata.Location=res[0].locationName;
          this.Occupantdata.ComplaintDescription=res[0].complaintDescription;
          this.Occupantdata.CreatedDate=res[0].createdDate;
          this.Occupantdata.ServiceTypeName=res[0].serviceTypeName;
          this.Occupantdata.AccomodationType=res[0].accommodationType;
          this.Occupantdata.AccomodationNo=res[0].accommodationNumber;
         this.Occupantdata.BuildingName=res[0].buildingName;
         this.Occupantdata.ComplexName=res[0].complexName;
        }
        
      });
  }

}
