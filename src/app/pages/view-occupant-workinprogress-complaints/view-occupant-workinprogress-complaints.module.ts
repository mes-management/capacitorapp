import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewOccupantWorkinprogressComplaintsPageRoutingModule } from './view-occupant-workinprogress-complaints-routing.module';

import { ViewOccupantWorkinprogressComplaintsPage } from './view-occupant-workinprogress-complaints.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewOccupantWorkinprogressComplaintsPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ViewOccupantWorkinprogressComplaintsPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ViewOccupantWorkinprogressComplaintsPageModule {}
