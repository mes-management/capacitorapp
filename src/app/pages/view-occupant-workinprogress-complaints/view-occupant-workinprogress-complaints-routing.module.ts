import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewOccupantWorkinprogressComplaintsPage } from './view-occupant-workinprogress-complaints.page';

const routes: Routes = [
  {
    path: '',
    component: ViewOccupantWorkinprogressComplaintsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewOccupantWorkinprogressComplaintsPageRoutingModule {}
