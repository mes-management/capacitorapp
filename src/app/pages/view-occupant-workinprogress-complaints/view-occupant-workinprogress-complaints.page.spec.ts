import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewOccupantWorkinprogressComplaintsPage } from './view-occupant-workinprogress-complaints.page';

describe('ViewOccupantWorkinprogressComplaintsPage', () => {
  let component: ViewOccupantWorkinprogressComplaintsPage;
  let fixture: ComponentFixture<ViewOccupantWorkinprogressComplaintsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOccupantWorkinprogressComplaintsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewOccupantWorkinprogressComplaintsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
