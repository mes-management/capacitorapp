import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { LoadingService } from 'src/app/providers/LoadingService';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-ge-rejected-occupants',
  templateUrl: './ge-rejected-occupants.page.html',
  styleUrls: ['./ge-rejected-occupants.page.scss'],
  providers:[OccupantRegistrationService]
})
export class GeRejectedOccupantsPage implements OnInit {
 Occupantdata={
    OccupantName: "",
    EmailID: "",
    MobileNumber: "",
    Location: "",
    Division: "",
    CreatedDate: "",
    ComplexName: "",
    BuildingName: "",
    AccomodationNo: "",
    Status:"",
    
        };
  
  constructor(public OccupantService:OccupantRegistrationService,private route :Router,public loadingController: LoadingController,public loading: LoadingService) { }

  ngOnInit() {
      let MobileNumber=localStorage.getItem('mobileNumber');
    this.OccupantService.RejectedOccupantDetailsByMono(MobileNumber).subscribe(res =>
      {
        if(res){
     
          this.Occupantdata.OccupantName=res[0].name;
          this.Occupantdata.EmailID=res[0].emailID;
          this.Occupantdata.MobileNumber=res[0].mobileNumber;
          this.Occupantdata.Location=res[0].location;
          this.Occupantdata.Division=res[0].officeType;
          this.Occupantdata.CreatedDate=res[0].createdDate;
          this.Occupantdata.ComplexName=res[0].complexName;
          this.Occupantdata.BuildingName=res[0].buildingName;
          this.Occupantdata.AccomodationNo=res[0].accommodationNumber;
          this.Occupantdata.Status=res[0].status;
        
         
        }
        
      });
  }

}
