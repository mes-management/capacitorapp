import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GeRejectedOccupantsPage } from './ge-rejected-occupants.page';

describe('GeRejectedOccupantsPage', () => {
  let component: GeRejectedOccupantsPage;
  let fixture: ComponentFixture<GeRejectedOccupantsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GeRejectedOccupantsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GeRejectedOccupantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
