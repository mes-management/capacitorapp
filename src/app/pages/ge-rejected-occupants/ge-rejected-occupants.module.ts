import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GeRejectedOccupantsPageRoutingModule } from './ge-rejected-occupants-routing.module';

import { GeRejectedOccupantsPage } from './ge-rejected-occupants.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeRejectedOccupantsPageRoutingModule
  ],
  declarations: [GeRejectedOccupantsPage]
})
export class GeRejectedOccupantsPageModule {}
