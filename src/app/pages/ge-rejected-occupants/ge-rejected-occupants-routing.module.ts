import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeRejectedOccupantsPage } from './ge-rejected-occupants.page';

const routes: Routes = [
  {
    path: '',
    component: GeRejectedOccupantsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeRejectedOccupantsPageRoutingModule {}
