import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewOccupantPendingComplaintsPageRoutingModule } from './view-occupant-pending-complaints-routing.module';

import { ViewOccupantPendingComplaintsPage } from './view-occupant-pending-complaints.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewOccupantPendingComplaintsPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ViewOccupantPendingComplaintsPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ViewOccupantPendingComplaintsPageModule {}
