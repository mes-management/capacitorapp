import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewOccupantPendingComplaintsPage } from './view-occupant-pending-complaints.page';

const routes: Routes = [
  {
    path: '',
    component: ViewOccupantPendingComplaintsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewOccupantPendingComplaintsPageRoutingModule {}
