import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewOccupantPendingComplaintsPage } from './view-occupant-pending-complaints.page';

describe('ViewOccupantPendingComplaintsPage', () => {
  let component: ViewOccupantPendingComplaintsPage;
  let fixture: ComponentFixture<ViewOccupantPendingComplaintsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOccupantPendingComplaintsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewOccupantPendingComplaintsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
