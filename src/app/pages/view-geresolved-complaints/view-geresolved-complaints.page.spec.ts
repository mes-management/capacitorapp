import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewGEResolvedComplaintsPage } from './view-geresolved-complaints.page';

describe('ViewGEResolvedComplaintsPage', () => {
  let component: ViewGEResolvedComplaintsPage;
  let fixture: ComponentFixture<ViewGEResolvedComplaintsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGEResolvedComplaintsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewGEResolvedComplaintsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
