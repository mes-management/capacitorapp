import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewGEResolvedComplaintsPageRoutingModule } from './view-geresolved-complaints-routing.module';

import { ViewGEResolvedComplaintsPage } from './view-geresolved-complaints.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewGEResolvedComplaintsPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ViewGEResolvedComplaintsPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ViewGEResolvedComplaintsPageModule {}
