import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewGEResolvedComplaintsPage } from './view-geresolved-complaints.page';

const routes: Routes = [
  {
    path: '',
    component: ViewGEResolvedComplaintsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewGEResolvedComplaintsPageRoutingModule {}
