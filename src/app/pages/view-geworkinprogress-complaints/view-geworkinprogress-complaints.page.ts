import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-view-geworkinprogress-complaints',
  templateUrl: './view-geworkinprogress-complaints.page.html',
  styleUrls: ['./view-geworkinprogress-complaints.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService],
  encapsulation: ViewEncapsulation.None
})
export class ViewGEWorkinprogressComplaintsPage implements OnInit {
  public columns: any;
  public rows: any;
  errorMessage: string; 
  constructor(private http: HttpClient,public OccupantService:OccupantRegistrationService,private route :Router
    ) {
      this.columns = [
        { name: 'complaintCode' },
        { name: 'ServiceTypeName' },
        // {name:'accommodationNumber'},
        // {name:'accommodationType'},
        // { name: 'locationName'},
        // { name: 'divisionName' },
        {name:'complexName'},
      //  {name:'status'},
        
      ];
  
       
    }
    ngOnInit() { 
      //let OccupantID= localStorage.getItem('occupantID');
      let USR_ID=localStorage.getItem('usR_ID')
    this.OccupantService.GetGEWorkinProgressComplaints(USR_ID).subscribe(res =>
      this.rows = res, 
   error => this.errorMessage = <any>error);
    }

    onActivate(event) {
      if(event.type == 'click') {
        let complaintCode=event.row.complaintCode;
        localStorage.setItem('complaintCode',complaintCode);
        this.route.navigateByUrl('geworking-progress-complaints')
      }
     
  }
}