import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewGEWorkinprogressComplaintsPage } from './view-geworkinprogress-complaints.page';

describe('ViewGEWorkinprogressComplaintsPage', () => {
  let component: ViewGEWorkinprogressComplaintsPage;
  let fixture: ComponentFixture<ViewGEWorkinprogressComplaintsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGEWorkinprogressComplaintsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewGEWorkinprogressComplaintsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
