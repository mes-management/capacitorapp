import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewGEWorkinprogressComplaintsPage } from './view-geworkinprogress-complaints.page';

const routes: Routes = [
  {
    path: '',
    component: ViewGEWorkinprogressComplaintsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewGEWorkinprogressComplaintsPageRoutingModule {}
