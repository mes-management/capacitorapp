import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewGEWorkinprogressComplaintsPageRoutingModule } from './view-geworkinprogress-complaints-routing.module';

import { ViewGEWorkinprogressComplaintsPage } from './view-geworkinprogress-complaints.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewGEWorkinprogressComplaintsPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ViewGEWorkinprogressComplaintsPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ViewGEWorkinprogressComplaintsPageModule {}
