import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewOccupantResolvedComplaintsPageRoutingModule } from './view-occupant-resolved-complaints-routing.module';

import { ViewOccupantResolvedComplaintsPage } from './view-occupant-resolved-complaints.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewOccupantResolvedComplaintsPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ViewOccupantResolvedComplaintsPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ViewOccupantResolvedComplaintsPageModule {}
