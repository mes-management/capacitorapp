import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewOccupantResolvedComplaintsPage } from './view-occupant-resolved-complaints.page';

const routes: Routes = [
  {
    path: '',
    component: ViewOccupantResolvedComplaintsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewOccupantResolvedComplaintsPageRoutingModule {}
