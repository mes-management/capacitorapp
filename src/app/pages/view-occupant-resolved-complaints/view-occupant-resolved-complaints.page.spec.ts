import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewOccupantResolvedComplaintsPage } from './view-occupant-resolved-complaints.page';

describe('ViewOccupantResolvedComplaintsPage', () => {
  let component: ViewOccupantResolvedComplaintsPage;
  let fixture: ComponentFixture<ViewOccupantResolvedComplaintsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOccupantResolvedComplaintsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewOccupantResolvedComplaintsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
