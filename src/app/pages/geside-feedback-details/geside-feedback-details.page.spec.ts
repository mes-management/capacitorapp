import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GESideFeedbackDetailsPage } from './geside-feedback-details.page';

describe('GESideFeedbackDetailsPage', () => {
  let component: GESideFeedbackDetailsPage;
  let fixture: ComponentFixture<GESideFeedbackDetailsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GESideFeedbackDetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GESideFeedbackDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
