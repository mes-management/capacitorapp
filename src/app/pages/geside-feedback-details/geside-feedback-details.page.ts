import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-geside-feedback-details',
  templateUrl: './geside-feedback-details.page.html',
  styleUrls: ['./geside-feedback-details.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService],
 
  encapsulation: ViewEncapsulation.None
})
export class GESideFeedbackDetailsPage implements OnInit {
  public columns: any;
  public rows: any;
  constructor( private http: HttpClient,public OccupantService:OccupantRegistrationService) { 
  this.columns = [
    { name: 'occupantName' },
    { name: 'occupantRemarks' },
    { name: 'occupantRating' }
  ];
}
 
ngOnInit() {
  let USR_ID=localStorage.getItem('usR_ID')
  this.OccupantService.ViewGESideFeedback(USR_ID).subscribe(res =>
   
   {
     this.rows=res;
     if(res){
      // this.rows = res[0]
      // alert(JSON.stringify(this.rows));
     }
   });
  
    
    
  }
onActivate(event) {
  
}
}
