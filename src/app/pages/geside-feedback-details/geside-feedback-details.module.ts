import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GESideFeedbackDetailsPageRoutingModule } from './geside-feedback-details-routing.module';

import { GESideFeedbackDetailsPage } from './geside-feedback-details.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GESideFeedbackDetailsPageRoutingModule, 
       NgxDatatableModule
  ],
  declarations: [GESideFeedbackDetailsPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class GESideFeedbackDetailsPageModule {}
