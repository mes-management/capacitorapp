import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GESideFeedbackDetailsPage } from './geside-feedback-details.page';

const routes: Routes = [
  {
    path: '',
    component: GESideFeedbackDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GESideFeedbackDetailsPageRoutingModule {}
