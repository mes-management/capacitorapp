import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { ComplaintRegistrationPageRoutingModule } from './complaint-registration-routing.module';

import { ComplaintRegistrationPage } from './complaint-registration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComplaintRegistrationPageRoutingModule
  ],
  declarations: [ComplaintRegistrationPage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class ComplaintRegistrationPageModule {}
