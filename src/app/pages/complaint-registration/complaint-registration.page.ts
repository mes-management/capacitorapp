import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-complaint-registration',
  templateUrl: './complaint-registration.page.html',
  styleUrls: ['./complaint-registration.page.scss'],
  providers:[AuthServiceService,OccupantRegistrationService]
 
})
export class ComplaintRegistrationPage implements OnInit {
  public self:boolean=false;
  public other:boolean=false;

  public AllData:boolean=false;
  emp:any;
  loading: any;
  photo:any;
 
  errorMessage: string; 
  locationData:any;
  AccomodationData:any;
  ServiceTypeData:any
  divisionData:any;
  subdivisionData:any;
  occupantdata:any;
  otheroccupantID:any;
  regData={ServiceTypeID:'', OccupantID:0,Status:'Pending',ComplaintDescription:'', FilePath:''};
  
  
  allselfdata={
    AccommodationType:"",AccommodationNumber:"",

  };
 
  selfdata: boolean;
  Description:any;

  constructor( 
    private route :Router,public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,public OccupantService:OccupantRegistrationService)
    
  {
  
   }

  ngOnInit() {

   this.getServiceType();
   this.getOccupant();
  
  }

getselfdetails()
{
  let SelfOccupantID=localStorage.getItem('occupantID');
//  alert(SelfOccupantID)
  this.OccupantService.GetSelfData(SelfOccupantID).subscribe(res =>{

  if(res){

    this.allselfdata.AccommodationType=res[0].accommodationType;
    this.allselfdata.AccommodationNumber=res[0].accommodationNumber;
    }
        
});
}


  getOccupant()
  {

    
      this.OccupantService.getOccupantslist().subscribe(res =>
        this.occupantdata = res, 
      
        error => this.errorMessage = <any>error); 
  }
  getAccomdationData(){
  
    this.OccupantService.AccomdationData1().subscribe(res =>
   this.AccomodationData = res, 
   error => this.errorMessage = <any>error);  
  
  }
  getServiceType(){
  
    this.OccupantService.ServiceType().subscribe(res =>
      this.ServiceTypeData = res, 
      error => this.errorMessage = <any>error);
  }
  onSelectChangeAccom(data :any){
   // this.regData.AccommodationID=data.accommodationID;
    // alert(this.regData.AccommodationID)
    //this.getOccupant(data.accommodationID);
   // this.getOccupant(this.regData.Acc_DetailsID);
  
 }

 onSelectChangeoccu(data:any){
  
  this.regData.OccupantID=data.occupantID;
  this.otheroccupantID=data.occupantID;

 }

  changeStatus(data:any)
  {

   
    if(data=="0")
    {
      this.AllData=true;
       this.self=true;
       this.other=false;
       this.selfdata=true;
       this.getselfdetails();
    }
    else{
      this.AllData=true;
      this.other=true;
      this.self=false;
      this.selfdata=false;
      
    }
  }
  
  onSelectChangeservicetype(data:any)
  {
     this.regData.ServiceTypeID=data.serviceTypeID;
  }
  onFileChoose(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    const pattern = /image-*/;
    const reader = new FileReader();

    if (!file.type.match(pattern)) {
      console.log('File format not supported');
      return;
    }
    reader.onload = () => {
      this.photo = reader.result.toString();
 
      this.regData.FilePath=this.photo;
    
   
    };
    reader.readAsDataURL(file);
    
  }

 
  doRegistration(){
  this.regData.ComplaintDescription=this.Description;
  if(this.selfdata==true)
  {
    this.regData.OccupantID=parseInt(localStorage.getItem('occupantID'));
  }
  else
  {
    this.regData.OccupantID=this.otheroccupantID;
  }
 
    this.OccupantService.AddComplaint(this.regData).then((result) => {
 
       alert("Your Complaint Request has been Submitted Successfully ");


       this.route.navigate(['view-occupant-pending-complaints'])
       .then(() => {
         window.location.reload();
        
       });


    //    this.route.navigateByUrl('view-occupant-pending-complaints');
    // this.AccomodationData="";
    //   this.regData.FilePath="";
     
    }, (err) => {
    
     
    });
  }

  }
