import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminViewOccupantDataPage } from './admin-view-occupant-data.page';

const routes: Routes = [
  {
    path: '',
    component: AdminViewOccupantDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminViewOccupantDataPageRoutingModule {}
