import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Data, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { LoadingService } from 'src/app/providers/LoadingService';
import { OccupantRegistrationService } from 'src/app/providers/occupant-registration.service';

@Component({
  selector: 'app-admin-view-occupant-data',
  templateUrl: './admin-view-occupant-data.page.html',
  styleUrls: ['./admin-view-occupant-data.page.scss'],
  providers:[OccupantRegistrationService],
  encapsulation: ViewEncapsulation.None
})
export class AdminViewOccupantDataPage implements OnInit {
 
  public data: Data;
  public columns: any;
  public rows: any;
  errorMessage:any;
  constructor(private http: HttpClient,public OccupantService:OccupantRegistrationService,private route :Router,public loading: LoadingService) {
    this.columns = [
     
      { name: 'name' },
      { name: 'mobileNumber' },
    //  { name: 'createdDate' },
      { name: 'status' },
     
    ];
   //this.getOccupantDetails();
  }
  ngOnInit() {
    this.getOccupantDetails();
  }
  
  onActivate(event) {
    if(event.type == 'click') {
       let mobileNumber=event.row.mobileNumber;
       localStorage.setItem('mobileNumber',mobileNumber);
       this.route.navigateByUrl('admin-approve-occupant');
    }
}
getOccupantDetails(){
  let USR_ID=localStorage.getItem('usR_ID');
  this.loading.present();
  this.OccupantService.GetOccupantDetailsByLocID(USR_ID).subscribe(res =>
    {
     
      this.rows=res;
      this.loading.dismiss();
  }, (err) => {
    this.loading.dismiss();
  }); 
}

}
