import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminViewOccupantDataPageRoutingModule } from './admin-view-occupant-data-routing.module';

import { AdminViewOccupantDataPage } from './admin-view-occupant-data.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxDatatableModule,
    AdminViewOccupantDataPageRoutingModule
  
  ],
  declarations: [AdminViewOccupantDataPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdminViewOccupantDataPageModule {}
