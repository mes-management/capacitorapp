import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';  
import { map } from "rxjs/operators";

@Component({
  selector: 'app-viewregisteredcom',
  templateUrl: './viewregisteredcom.page.html',
  styleUrls: ['./viewregisteredcom.page.scss'],
})
export class ViewregisteredcomPage implements OnInit {

  posts:any;
  constructor(public http:Http) { 
    this.http.get('http://meswebapi.neemus.in/api/Complaint/GetRegisteredCom').pipe(map(res => res.json())).subscribe(data => {
      console.log(data);
     // alert(JSON.stringify(data))
      this.posts=data;
    }
    );
  }

  ngOnInit() {
  }

}
