import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewregisteredcomPage } from './viewregisteredcom.page';

const routes: Routes = [
  {
    path: '',
    component: ViewregisteredcomPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewregisteredcomPageRoutingModule {}
