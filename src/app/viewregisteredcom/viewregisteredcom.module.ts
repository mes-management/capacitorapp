import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewregisteredcomPageRoutingModule } from './viewregisteredcom-routing.module';

import { ViewregisteredcomPage } from './viewregisteredcom.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewregisteredcomPageRoutingModule
  ],
  declarations: [ViewregisteredcomPage]
})
export class ViewregisteredcomPageModule {}
