export class LocationModel
{
    LocationID:number;
    LocationName:string;
    
}
export class DivisionModel
{
    DivisionID:number;
    DivisionName:string;
    
}
export class SubDivisionModel
{
    SubDivisionId:number;
    SubDivisionName:string;
    
}