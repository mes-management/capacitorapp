import { Injectable } from '@angular/core';  
import { Observable } from 'rxjs';
import { LocationModel } from './OccupantModel';
import { Http } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { stringify } from 'querystring';

let apiUrl = 'http://meswebapi.neemus.in/api/';
@Injectable()  
export class OccupantRegistrationService  
{  
    constructor(private http: HttpClient) { } 

    getAccomdationTypes(data:any): Observable<any> {  
        return this.http.get(apiUrl+'Accomodation/GetdistinctAccomodationType?ComplexID='+data);  
    }
    getlocationdetails(OfficeID:any): Observable<any> {  
        return this.http.get(apiUrl+'Location/GetLocationsByDivision?OfficeID='+OfficeID);  
    }
    getcomplexdetails(LocationID:any): Observable<any> {  
        return this.http.get(apiUrl+'Location/GetComplexByLocationID?LocationID='+LocationID);  
    }
    AccomdationData(ComplexID:any,BuildingName:any): Observable<any> {  
        return this.http.get(apiUrl+'Accomodation/GetAccomodationByComplexID?ComplexID='+ComplexID+'&&BuildingName='+BuildingName);  
    }
    getDivisiondetails(): Observable<any> {  
        return this.http.get(apiUrl+'Division/GetDivisions');
           
    }
    getSubDivisiondetails(divisionID:any): Observable<any> {  
        return this.http.get(apiUrl+'SubDivision/GetSubDivisionDetailsById?divisionid='+divisionID);
           
    }
    checkMobileExists(MobileNumber:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/checkMobileExists?MobileNumber='+MobileNumber);
           
    }
    checkOccupantStatus(OccupantID:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/checkOccupantStatus?OccupantID='+OccupantID);
           
    }
    AddOccupant(data){
       //alert(JSON.stringify(data))
        const promise2 = this.http.post(apiUrl+'OccupantDetails/InsertOccupantDetails',(data)).toPromise(); 
        promise2.then((data)=>{
        }).catch((error)=>{
        });
        return promise2;
      }

      GetOccupantDetailsByLocID(USR_ID:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/GetOccupantDetailsByLocID?USR_ID='+USR_ID);  
    }
    ApprovedOccupantDetails(USR_ID:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/ApprovedOccupantDetails?USR_ID='+USR_ID);  
<<<<<<< HEAD
    }
    ViewApprovedOccupantDetails(MobileNumber:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/ApprovedOccupantDetailsByMobileNumber?MobileNumber='+MobileNumber);  
    }

    AccomdationData11(ComplexID:any): Observable<any> {  
        return this.http.get(apiUrl+'Accomodation/GetAccomodationByComplexID1?ComplexID='+ComplexID);  
    }
    // AccomdationData1(): Observable<any> {  
    //     return this.http.get(apiUrl+'Accomodation/GetAccomodationDetails');  
    // }
    
    RejectedOccupantDetails(USR_ID:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/RejectedOccupantDetails?USR_ID='+USR_ID);  
    }
     RejectedOccupantDetailsByMono(MobileNumber:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/RejectedOccupantDetailsByMobilenumber?MobileNumber='+MobileNumber);  
=======
    }
    ViewApprovedOccupantDetails(MobileNumber:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/ApprovedOccupantDetailsByMobileNumber?MobileNumber='+MobileNumber);  
    }


    RejectedOccupantDetails(USR_ID:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/RejectedOccupantDetails?USR_ID='+USR_ID);  
>>>>>>> 433948a77c4b4afad20f042cd316eb052ae61df0
    }

    GetOccupantDetailsByOccupantID(OccupantID:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/GetOccupantDetailsByOccupantID?OccupantID='+OccupantID);  
    }
    GetOccupantDetailsByMobileNumber(MobileNumber:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/GetOccupantDetailsByMobileNumber?MobileNumber='+MobileNumber);  
    }
    UpdateOccupantApprovalStatus(regData:any): Observable<any> {  
        return this.http.get(apiUrl+'OccupantDetails/UpdateOccupantApprovalStatus?OccupantID='+regData.OccupantID+'&&Remarks='+regData.Remarks+'&&Status='+regData.Status);  
    }

   //pavani 
        AccomdationData1(): Observable<any> {  
              return this.http.get(apiUrl+'Accomodation/GetAccomodationDetails');  
          }
          ServiceType(): Observable<any> {  
             
              return this.http.get(apiUrl+'ServiceType/GetServiceType');  
          }
       getOccupantslist(): Observable<any> {  
             //alert('hh')
              return this.http.get(apiUrl+'OccupantDetails/GetOccupantDetailsByAccommodation');
          }
        
          
           viewAllFeedbackDetails(OccupantID:any): Observable<any> {  
<<<<<<< HEAD
             
              return this.http.get(apiUrl+'Feedback/ViewFeedbackDetails?OccupantID='+OccupantID);
          }

          viewOccupantFeedbackData(ComplaintCode:any): Observable<any> {  
           //   alert(ComplaintCode)
            return this.http.get(apiUrl+'Feedback/ViewFeedbackDetailsByComplaintCode?ComplaintCode='+ComplaintCode);
        }

          GetGEPendingComplaintsData(ComplaintCode:any):Observable<any>{
            // alert(ComplaintCode)
             return this.http.get(apiUrl+'ComplaintDetails/GEviewpendingcomplaints?ComplaintCode='+ComplaintCode); 
         }

GetGEViewResolvedComplaintsData(ComplaintCode:any):Observable<any>{
            //alert(ComplaintCode)
             return this.http.get(apiUrl+'ComplaintDetails/ViewGEResolvedComplaintsByComplaintCode?ComplaintCode='+ComplaintCode); 
         }

         
GetOccupantPendingComplaintsData(ComplaintCode:any):Observable<any>{
    //alert(ComplaintCode)
     return this.http.get(apiUrl+'ComplaintDetails/OccupantPendingComplaintsByComplaintCode?ComplaintCode='+ComplaintCode); 
 }
    
 
        
// GetOccupantFeedbackData(ComplaintCode:any):Observable<any>{
//     //alert(ComplaintCode)
//      return this.http.get(apiUrl+'Feedback/ViewFeedbackDetailsByComplaintCode?ComplaintCode='+ComplaintCode); 
//  }
GetOccupantResolvedComplaintsData(ComplaintCode:any):Observable<any>{
    //alert(ComplaintCode)
     return this.http.get(apiUrl+'ComplaintDetails/OccupantResolvedComplaintsByComplaintCode?ComplaintCode='+ComplaintCode); 
 }


 GetOccupantWorkInComplaintsData(ComplaintCode:any):Observable<any>{
    //alert(ComplaintCode)
     return this.http.get(apiUrl+'ComplaintDetails/OccupantWorkinComplaintsByComplaintCode?ComplaintCode='+ComplaintCode); 
 }


         GetGEWorkinprogressComplaintsdata(ComplaintCode:any):Observable<any>{
            // alert(ComplaintCode)
             return this.http.get(apiUrl+'ComplaintDetails/GEviewworkinprogresscomplaints?ComplaintCode='+ComplaintCode); 
         }


         AddGEWorkinprogress(data){
        //  alert(JSON.stringify(data))


            const promise2 = this.http.post(apiUrl+'ComplaintDetails/GEUpdatetworkinProgressComplaints',(data)).toPromise(); 
            promise2.then((data)=>{
            }).catch((error)=>{
            });
            return promise2;
         }

         AddGeapprovedComplaints(regData:any){
              //alert(JSON.stringify(data));
             //  const promise2 = this.http.post(apiUrl+'ComplaintDetails/GEUpdatetComplaints',(data)).toPromise(); 
             //  promise2.then((data)=>{
             //  }).catch((error)=>{
             //  });
             //  return promise2; 
             return this.http.get(apiUrl+'ComplaintDetails/GEUpdatetComplaints?ComplaintCode='+regData.ComplaintCode+'&&Comments='+regData.Comments+'&&Status='+regData.Status+'&&USR_ID='+regData.USR_ID);
       
            }
         AddComplaint(data){
              // alert(JSON.stringify(data));
               const promise2 = this.http.post(apiUrl+'ComplaintDetails/InsertComplaints',(data)).toPromise(); 
               promise2.then((data)=>{
               }).catch((error)=>{
               });
               return promise2;
           }
           AddFeedbackInfo(data){
             // alert(JSON.stringify(data))
                const promise2 = this.http.post(apiUrl+'Feedback/InsertFeedback',(data)).toPromise(); 
                promise2.then((data)=>{
                }).catch((error)=>{
                });
                return promise2;
            }

            GetAllPendingComplaints(USR_ID:any):Observable<any>
            {  
               
                return this.http.get(apiUrl+'ComplaintDetails/GetPendingComplaintsByLocID?USR_ID='+USR_ID);
            }
            getComplaintFeedback(OccupantID:any): Observable<any> {  
          
                return this.http.get(apiUrl+'ComplaintFeedback/GetComplaintFeedback?OccupantID='+OccupantID);
            }
            GetSelfData(SelfOccupantID:any): Observable<any> {  
                // alert(SelfOccupantID)
                     return this.http.get(apiUrl+'OccupantDetails/GetOccupantDetailsByOccupantID1?OccupantID='+SelfOccupantID);
                 }




                 GetOccupantPendingComplaints(OccupantID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/OccupantPendingComplaints?OccupantID='+OccupantID);
                 }
       
                 GetOccupantResolvedComplaints(OccupantID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/OccupantResolvedComplaints?OccupantID='+OccupantID);
                 }
                 GetOccupantWorkinprogressComplaints(OccupantID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/OccupantWorkinprogressComplaints?OccupantID='+OccupantID);
                 }
       
                 GetGEWorkinProgressComplaints(USR_ID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/ViewGEWorkinprogressComplaintsByLocID?USR_ID='+USR_ID);
                 }
       
                 GetGEResolvedComplaints(USR_ID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/ViewGEResolvedComplaintsByLocID?USR_ID='+USR_ID);
                 }
                 ViewGESideFeedback(USR_ID:any): Observable<any> {  
             
                    return this.http.get(apiUrl+'Feedback/ViewFeedbackDetailsByUSRID?USR_ID='+USR_ID);
                }
  


















                
=======
             
              return this.http.get(apiUrl+'Feedback/ViewFeedbackDetails?OccupantID='+OccupantID);
          }
          GetGEPendingComplaintsData(ComplaintCode:any):Observable<any>{
            // alert(ComplaintCode)
             return this.http.get(apiUrl+'ComplaintDetails/GEviewpendingcomplaints?ComplaintCode='+ComplaintCode); 
         }


         GetGEWorkinprogressComplaintsdata(ComplaintCode:any):Observable<any>{
            // alert(ComplaintCode)
             return this.http.get(apiUrl+'ComplaintDetails/GEviewworkinprogresscomplaints?ComplaintCode='+ComplaintCode); 
         }


         AddGEWorkinprogress(data){
        //  alert(JSON.stringify(data))


            const promise2 = this.http.post(apiUrl+'ComplaintDetails/GEUpdatetworkinProgressComplaints',(data)).toPromise(); 
            promise2.then((data)=>{
            }).catch((error)=>{
            });
            return promise2;
         }

         AddGeapprovedComplaints(regData:any){
              //alert(JSON.stringify(data));
             //  const promise2 = this.http.post(apiUrl+'ComplaintDetails/GEUpdatetComplaints',(data)).toPromise(); 
             //  promise2.then((data)=>{
             //  }).catch((error)=>{
             //  });
             //  return promise2; 
             return this.http.get(apiUrl+'ComplaintDetails/GEUpdatetComplaints?ComplaintCode='+regData.ComplaintCode+'&&Comments='+regData.Comments+'&&Status='+regData.Status+'&&USR_ID='+regData.USR_ID);
       
            }
         AddComplaint(data){
              // alert(JSON.stringify(data));
               const promise2 = this.http.post(apiUrl+'ComplaintDetails/InsertComplaints',(data)).toPromise(); 
               promise2.then((data)=>{
               }).catch((error)=>{
               });
               return promise2;
           }
           AddFeedbackInfo(data){
             // alert(JSON.stringify(data))
                const promise2 = this.http.post(apiUrl+'Feedback/InsertFeedback',(data)).toPromise(); 
                promise2.then((data)=>{
                }).catch((error)=>{
                });
                return promise2;
            }

            GetAllPendingComplaints(USR_ID:any):Observable<any>
            {  
               
                return this.http.get(apiUrl+'ComplaintDetails/GetPendingComplaintsByLocID?USR_ID='+USR_ID);
            }
            getComplaintFeedback(OccupantID:any): Observable<any> {  
          
                return this.http.get(apiUrl+'ComplaintFeedback/GetComplaintFeedback?OccupantID='+OccupantID);
            }
            GetSelfData(SelfOccupantID:any): Observable<any> {  
                // alert(SelfOccupantID)
                     return this.http.get(apiUrl+'OccupantDetails/GetOccupantDetailsByOccupantID1?OccupantID='+SelfOccupantID);
                 }




                 GetOccupantPendingComplaints(OccupantID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/OccupantPendingComplaints?OccupantID='+OccupantID);
                 }
       
                 GetOccupantResolvedComplaints(OccupantID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/OccupantResolvedComplaints?OccupantID='+OccupantID);
                 }
                 GetOccupantWorkinprogressComplaints(OccupantID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/OccupantWorkinprogressComplaints?OccupantID='+OccupantID);
                 }
       
                 GetGEWorkinProgressComplaints(USR_ID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/ViewGEWorkinprogressComplaintsByLocID?USR_ID='+USR_ID);
                 }
       
                 GetGEResolvedComplaints(USR_ID:any):Observable<any>
                 {  
                    
                     return this.http.get(apiUrl+'ComplaintDetails/ViewGEResolvedComplaintsByLocID?USR_ID='+USR_ID);
                 }
                 ViewGESideFeedback(USR_ID:any): Observable<any> {  
             
                    return this.http.get(apiUrl+'Feedback/ViewFeedbackDetailsByUSRID?USR_ID='+USR_ID);
                }
    // private handleError(error: Response | any) {  
    //     let errMsg: string;  
    //     if (error instanceof Response) {  
    //         const body = error.json() || '';  
    //         const err = body.error || JSON.stringify(body);  
    //         errMsg = `${error.status} - ${error.statusText || ''} ${err}`;  
    //     } else {  
    //         errMsg = error.message ? error.message : error.toString();  
    //     }  
    //     console.error(errMsg);  
    //     return Observable.throw(errMsg);  
    // }  
>>>>>>> 433948a77c4b4afad20f042cd316eb052ae61df0
}