import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { Http, Response, RequestOptions, Headers } from '@angular/http';  
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from "@angular/common/http";
//import {  ServiceModel} from '../app/complaint-registration/DepartmentModel';  

let apiUrl='http://meswebapi.neemus.in/api/';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http: HttpClient) { }

  // getService(): Observable<any> {  
  //   return this.http.get(apiUrl+'ServiceType/GetServiceType').pipe(      
  //     map((results:any) =>results )  
     
  //   ); 
  //   }
//     return this.http.get(apiUrl+'ServiceType/GetServiceType').map(results => results.json())  
//         .catch(this.handleError);  
// }

 /* Generated Method for the Login provider. */

 login(credentials) {
  return new Promise((resolve, reject) => {
    const headers = new HttpHeaders({'Content-Type': 'application/json' })
     // headers.append('Content-Type', 'application/json');
      //alert(credentials)
      this.http.post(apiUrl+'Login/LoginDetails',(credentials), { headers } )
        .subscribe((res:any) => {
          resolve(res); 
        }, (err) => {
          reject(err);
        });
  });
}
 
get() {
  return new Promise((resolve, reject) => {
    const headers = new HttpHeaders({'Content-Type': 'application/json' })
    headers.append('Content-Type', 'application/json');
    this.http.get(apiUrl+'ServiceType/GetServiceType')
      .subscribe((res:any) => {
        resolve(res);
       // alert(apiUrl)
      }, (err) => {
        reject(err);
      });
  });
}

//Complaint Registration 
complaintregister(data) {
  //alert(data)
  return new Promise((resolve, reject) => {
    const headers = new HttpHeaders({'Content-Type': 'application/json' })
      headers.append('Content-Type', 'text/plain');
      this.http.post(apiUrl+'Complaint/InsertComplaintDetails', (data),{headers})
        .subscribe(res => {
         (resolve(res));
        }, (err) => {
          reject(err);
        });
  });
}

private handleError(error: Response | any) {  
  let errMsg: string;  
  if (error instanceof Response) {  
      const body = error.json() || '';  
      const err = body.error || JSON.stringify(body);  
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;  
  } else {  
      errMsg = error.message ? error.message : error.toString();  
  }  
  console.error(errMsg);  
  return Observable.throw(errMsg);  
}  
}
