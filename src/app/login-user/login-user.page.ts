import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { AuthServiceService } from 'src/app/providers/auth-service.service';
import { Http,Headers } from '@angular/http';  
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingService } from '../providers/LoadingService';
import { OccupantRegistrationService } from '../providers/occupant-registration.service';
//import { LoadingController, ToastController } from 'ionic-angular';
//import { Events } from 'ionic-angular';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.page.html',
  styleUrls: ['./login-user.page.scss'],
  providers:[OccupantRegistrationService],
  encapsulation: ViewEncapsulation.None
})
export class LoginUserPage implements OnInit {
  loginForm: FormGroup;
  isSubmitted = false;
  mobileAlredyExist:any;
  errorMessage:any;
  locationData:any;
  AccomodationData:any;
  divisionData:any;
  subdivisionData:any;
  isoccupanthidden = false;
  isGEhidden=false;
  regData = {USR_LOGIN:'',USR_PASSWORD:''}
  data:any;
  USR_LOGIN:any;
  USR_PASSWORD:any;
  pwdIcon = "eye-outline";
  showPwd = false;


  constructor(private formBuilder: FormBuilder,private route :Router,public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,public AuthServiceProvider: AuthServiceService,
    public http:Http,public loading: LoadingService,public OccupantService:OccupantRegistrationService) 
    {
     
  }


  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      MobileNumber: ['', [Validators.required ]],
      Password: ['', [Validators.required]],
     
    })  
  }
  get errorControl() {
    return this.loginForm.controls;
  }
  togglePwd() {
    this.showPwd = !this.showPwd;
    this.pwdIcon = this.showPwd ? "eye-off-outline" : "eye-outline";
  }
  occupantregistration(){
    this.route.navigate(['/occupantregistration']);
  }
  Login()
  {
    this.isSubmitted = true;
    if (!this.loginForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      this.loading.present();
      this.regData.USR_LOGIN=this.loginForm.value.MobileNumber;
      this.regData.USR_PASSWORD=this.loginForm.value.Password;
     var headers = new Headers();
    // headers.append('Access-Control-Allow-Origin' , '*');
    // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    // headers.append('content-type','application/json; charset=utf-8');
    this.http.post('http://meswebapi.neemus.in/api/Login/LoginDetails',(this.regData),{headers: headers})
    .subscribe(res => {
      this.loading.dismiss();
     // resolve(res.json());
     this.data = res.json();
    //alert(JSON.stringify(this.data))
   if(this.data.length>0){
    localStorage.setItem('usertype', (this.data[0].usR_TYPE));
    localStorage.setItem('username', (this.regData.USR_LOGIN));
   // localStorage.setItem('occupantID',(this.data[0].occupantID));
    localStorage.setItem('usR_ID',(this.data[0].usR_ID))
    if(this.data[0].usR_TYPE=="Occupant"){
      this.OccupantService.checkOccupantStatus(this.data[0].occupantID).subscribe(res =>
        {
          if(res=="Pending"){
            alert("Your Registration is not yet Approved . Please Try login After Sometime.");
          }
          else if(res=="Approved"){
        localStorage.setItem('username', (this.regData.USR_LOGIN));
        localStorage.setItem('occupantID',(this.data[0].occupantID));
       //this.route.navigate(['/dashboard']);
       localStorage.setItem('eventName', "0");
       this.route.navigate(['/view-occupant-pending-complaints'])
      
       .then(() => {
         window.location.reload();
        
       });
      }
          else if(res=="Deactive"){
            alert("Occupant Deactivated");
          }
        });
     
      }
  else if(this.data[0].usR_TYPE=="GE")  
  {
    localStorage.setItem('eventName', "1");
    let userid=this.data[0].usR_ID;
    localStorage.setItem('UserId',userid);
    this.route.navigate(['/admin-view-occupant-data'])
    .then(() => {
      window.location.reload();
     
    });
  }
   }
  else{
        alert("Invalid User Credentials")
      }
    }, (err) => {
      //reject(err);
      this.loading.dismiss();
    });

  //  // this.showLoader();
  //     this.AuthServiceProvider.login(JSON.stringify(this.regData)).then((result) => {
  //     //this.loading.dismiss();
  //     this.data = result;
  //     alert(this.data)
  //    //this.username=result;
  //     if(result!=0){
  //    // localStorage.setItem('username', (this.regData.USR_LOGIN));
  //     //this.events.publish('eventName', this.regData.logintype);
  //    // this.route.navigate(['/welcome']);
  //    this.route.navigate(['/visitorregister']);
    
  //     }
    
  //     else{
  //       alert("Invalid User Credentials")
  //     }
  //     localStorage.setItem('token', this.data.access_token);
     
  //   }, (err) => {
  //     this.loading.dismiss();
  //     this.presentToast(err);
  //   });
  }
}
  // showLoader(){
  //   this.loading = this.loadingCtrl.create({
  //       //content: 'Authenticating...'
  //   });

  //   this.loading.present();
  // }

  // presentToast(msg) {
  //   let toast = this.toastCtrl.create({
  //     message: msg,
  //     duration: 3000,
  //     position: 'bottom',
  //    // dismissOnPageChange: true
  //   });

  //   // toast.onDidDismiss(() => {
  //   //   console.log('Dismissed toast');
  //   // });

  //   // toast.present();
  // }



   
}

