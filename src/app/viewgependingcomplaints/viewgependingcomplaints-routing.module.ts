import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewgependingcomplaintsPage } from './viewgependingcomplaints.page';

const routes: Routes = [
  {
    path: '',
    component: ViewgependingcomplaintsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewgependingcomplaintsPageRoutingModule {}
