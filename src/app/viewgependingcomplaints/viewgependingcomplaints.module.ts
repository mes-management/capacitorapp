import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewgependingcomplaintsPageRoutingModule } from './viewgependingcomplaints-routing.module';

import { ViewgependingcomplaintsPage } from './viewgependingcomplaints.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewgependingcomplaintsPageRoutingModule
  ],
  declarations: [ViewgependingcomplaintsPage]
})
export class ViewgependingcomplaintsPageModule {}
