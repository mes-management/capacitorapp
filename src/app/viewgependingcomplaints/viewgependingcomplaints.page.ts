import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';  
import { map } from "rxjs/operators";

@Component({
  selector: 'app-viewgependingcomplaints',
  templateUrl: './viewgependingcomplaints.page.html',
  styleUrls: ['./viewgependingcomplaints.page.scss'],
})
export class ViewgependingcomplaintsPage implements OnInit {

  posts:any;
  posts1:any;
  constructor(public http:Http) { 

    this.http.get('http://meswebapi.neemus.in/api/Complaint/GetPendingCom').pipe(map(res => res.json())).subscribe(data => {
      console.log(data);
     // alert(JSON.stringify(data))
      this.posts=data;
    }
    );

    this.http.get('https://meswebapi.neemus.com/api/Complaint/GetAssignedCom').pipe(map(res => res.json())).subscribe(data1 => {
      console.log(data1);
     // alert(JSON.stringify(data1))
      this.posts1=data1;
    }
    );

  }

  ngOnInit() {
  }

}
